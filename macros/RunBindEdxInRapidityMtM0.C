//This macro runs the compiled BindEdxInRapidityMtM0.cxx in
// src/pidCalibration/BindEdxInRapidityMtM0.cxx

void RunBindEdxInRapidityMtM0(TString inputFile, TString outputFile, TString starLibrary,
			      Double_t energy, TString eventConfig, Long64_t nEvents=-1){

  //Load the Necessary Libraries
  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/DavisDstReader_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/usefulDataStructs_cxx.so");
  
  gSystem->Load("../bin/BindEdxInRapidityMtM0_cxx.so");

  SetVariableUserCuts(energy,eventConfig,starLibrary);
  
  BindEdxInRapidityMtM0(inputFile,outputFile,nEvents);
    
}
