void drawPidCurves(TString STARLIBRARY){

  gSystem->Load("../bin/ParticleInfo_cxx.so");

  ParticleInfo *particleInfo = new ParticleInfo(STARLIBRARY,true);

  TCanvas *tpcCanvas = new TCanvas("tpcCanvas","tpcCanvas",20,20,800,600);
  tpcCanvas->SetLogy();
  tpcCanvas->SetLogx();
  tpcCanvas->SetRightMargin(.05);
  tpcCanvas->SetTopMargin(.05);
  tpcCanvas->SetTicks(1,1);
  
  TH1F *tpcFrame = tpcCanvas->DrawFrame(.01,1.1,1000,300);
  tpcFrame->SetTitle(";p (GeV/c);dE/dx (KeV/cm)");
  tpcFrame->GetYaxis()->SetTitleOffset(1.15);
  tpcFrame->GetXaxis()->SetTitleOffset(1.15);
  tpcFrame->SetTitleFont(62,"X");
  tpcFrame->SetTitleFont(62,"Y");

  TPaveText *tpcTitle = new TPaveText(.467,.85,.92,.92,"brNDC");
  tpcTitle->SetTextSize(.04);
  tpcTitle->SetFillColor(kWhite);
  tpcTitle->SetBorderSize(0);
  tpcTitle->SetTextAlign(12);
  tpcTitle->AddText("PID Using Energy Loss in the TPC");
  tpcTitle->Draw("SAME");
  
  TCanvas *tofCanvas = new TCanvas("tofCanvas","tofCanvas",20,20,800,600);
  tofCanvas->SetLogy();
  tofCanvas->SetLogx();
  tofCanvas->SetRightMargin(.05);
  tofCanvas->SetTopMargin(.05);
  tofCanvas->SetTicks(1,1);
  
  TH1F *tofFrame = tofCanvas->DrawFrame(.01,.5,10,300);
  tofFrame->SetTitle(";p (GeV/c);1/#beta");
  tofFrame->GetYaxis()->SetTitleOffset(1.15);
  tofFrame->GetXaxis()->SetTitleOffset(1.15);
  tofFrame->SetTitleFont(62,"X");
  tofFrame->SetTitleFont(62,"Y");

  TPaveText *tofTitle = new TPaveText(.53,.85,.92,.92,"brNDC");
  tofTitle->SetTextSize(.04);
  tofTitle->SetFillColor(kWhite);
  tofTitle->SetBorderSize(0);
  tofTitle->SetTextAlign(12);
  tofTitle->AddText("PID Using Velocity in the ToF");
  tofTitle->Draw("SAME");


  TLegend *leg = new TLegend(.64,.69,.93,.85);
  leg->SetBorderSize(0);
  leg->SetFillColor(kWhite);
  leg->SetTextSize(.035);
  leg->SetNColumns(2);
  
  const int nParticles = particleInfo->GetNumberOfDefinedParticles();
  for (int iParticle=0; iParticle<nParticles; iParticle++){

    if (iParticle == MUON || iParticle == ALPHA || iParticle == HELION)
      continue;

    leg->AddEntry(particleInfo->GetBichselFunction(iParticle),
		  Form("%s",particleInfo->GetParticleName(iParticle).Data()),"L");
    
    tpcCanvas->cd();
    particleInfo->GetBichselFunction(iParticle)->SetLineWidth(3);
    particleInfo->GetBichselFunction(iParticle)->Draw("SAME");

    tofCanvas->cd();
    particleInfo->GetInverseBetaFunction(iParticle)->SetLineWidth(3);
    particleInfo->GetInverseBetaFunction(iParticle)->Draw("SAME");
    
  }//End of loop over particles

  tofCanvas->cd();
  leg->Draw("SAME");

  tpcCanvas->cd();
  leg->Draw("SAME");
  
}
