#include "../inc/globalDefinitions.h"

//__________________________________________________________________________
TF1 *funcToScale[nRapidityBins];
Double_t scaleFunction(Double_t *x, Double_t *par){

  Double_t xx = x[0];

  Double_t scale = par[0];
  Int_t yIndex = (int)par[1];
  
  return funcToScale[yIndex]->Eval(xx) * scale;

}

//___________________________________________________________________________
void RemovePointsWithLargeErrors(TGraphErrors *spectrum, Double_t maxRelativeError=.1){

  //Loop Over the Points of the spectrum. Remove any point which is found
  //to have a relativeError larger than maxRelativeError
  for (int iPoint=spectrum->GetN()-1; iPoint>=0; iPoint--){

    if (spectrum->GetEY()[iPoint] / spectrum->GetY()[iPoint] > maxRelativeError)
      spectrum->RemovePoint(iPoint);
  }

}

void DrawAllRapiditySpectra(TString spectraFile, TString eventConfig, TString system, Double_t energy, Int_t speciesIndex, 
			    Int_t charge, Int_t centIndex, Double_t midY, Double_t startmTm0 = 0.0,
			    Bool_t corrected=false){

  Bool_t save = false;        //Should the canvas be saved?
  Double_t scaleFactor = 2.5; //The factor to scale the non- midrapidity spectra by
  Int_t yMin = 18;            //Minimum rapidity bin index to draw //Collider 11, PosY 18, NegY
  Int_t yMax = 40;            //Maximum rapidity bin index to draw //Collider 29, PosY 33, NegY
  Double_t maxmTm0=1.4;       //Maximum mT-m0 value to draw

  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  SetVariableUserCuts(energy,eventConfig,"");
  ParticleInfo *particleInfo = new ParticleInfo();

  yMin = GetMinRapidityIndexOfInterest(speciesIndex);
  yMax = GetMaxRapidityIndexOfInterest(speciesIndex);
  
  Double_t yScaleMin(0), yScaleMax(0);
  if (speciesIndex == 2){
    if (eventConfig.Contains("ColliderCenter")){
      yScaleMin = .000025;
      yScaleMax = 30000;
    }
    else if (eventConfig.Contains("ColliderPosY")){
      yScaleMin = .0025;
      yScaleMax = 300000000;
    }
  }
	if (eventConfig.Contains("FixedTarget2015")){
		yScaleMin = 0.000025;
		yScaleMax = 300000000;
	}
  
  //Load the root file
  TFile *file = new TFile(spectraFile,"READ");

  //Create the Spectra Name
  TString type = "raw";
  TString Type = "Raw";
  TString name = "Uncorrected";
  if (corrected){
    type = "corrected";
    Type = "Corrected";
    name = "Corrected";
  }

  TString speciesName = particleInfo->GetParticleName(speciesIndex,charge);
  const int nCentralityBins = GetNCentralityBins();
  Int_t midRapidityIndex = GetRapidityIndex(midY);
  cout <<"Mid Rapidity Index: " <<midRapidityIndex <<endl;
  
  //Create Array of TGraphErrors
  TGraphErrors *spectra[nRapidityBins];
  TGraphErrors *spectraTOF[nRapidityBins];
  TGraphErrors *scaledSpectra[nRapidityBins];
  TGraphErrors *scaledSpectraTOF[nRapidityBins];
  TF1 *spectraFit[nRapidityBins];
  TF1 *scaledSpectraFit[nRapidityBins];
  TPaveText *label[nRapidityBins];
 
  //Loop Over the centrality Bins
  for (int iCentBin=0; iCentBin < nCentralityBins; iCentBin++){

    TCanvas *canvas = NULL;
    if (iCentBin != centIndex){
      cout <<"Skipping Centrality Bin: " <<iCentBin <<endl;
      continue;
    }

    //Create the Canvas and Frame
    canvas = new TCanvas(Form("%s%02d_%s_%s_Cent%d",
			      system.Data(),(int)energy,speciesName.Data(),
			      eventConfig.Data(),iCentBin),"title",150*(iCentBin),20,600,800);

    canvas->SetTopMargin(.05);
    canvas->SetRightMargin(.05);
    canvas->SetLeftMargin(.17);
    canvas->SetLogy();
    canvas->SetTicks(1,1);

    /*
    if (spectraTOF[yIndex] && spectraTOF[yIndex]->GetN() == 0){
      delete spectraTOF[yIndex];
      spectraTOF[yIndex] = NULL;
    }
    */
    
    TH1F *frame = canvas->DrawFrame(0,yScaleMin,maxmTm0+.4,yScaleMax);
    frame->GetXaxis()->SetTitle(Form("m_{T}-m_{%s} (GeV/c^{2})",
				     particleInfo->GetParticleSymbol(speciesIndex).Data()));
    frame->GetYaxis()->SetTitle("#frac{1}{N_{Evt}}#times#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy} (GeV/c^{2})^{2}");
    frame->GetYaxis()->SetTitleOffset(2.2);
    frame->GetYaxis()->SetTitleFont(63);
    frame->GetYaxis()->SetTitleSize(25);
    frame->GetXaxis()->SetTitleFont(63);
    frame->GetXaxis()->SetTitleSize(25);
    frame->GetXaxis()->SetTitleOffset(1.3);

    canvas->Update();
    
    //Loop Over the rapidity bins
    for (Int_t yIndex=yMin; yIndex<=yMax; yIndex++){
      
      spectra[yIndex] = NULL;
      scaledSpectra[yIndex] = NULL;

      TString spectraName(Form("%sSpectra_%s_Cent%02d_yIndex%02d",
			       type.Data(),speciesName.Data(),iCentBin,yIndex));
      TString spectraNameTOF(Form("%sSpectraTOF_%s_Cent%02d_yIndex%02d",
				  type.Data(),speciesName.Data(),iCentBin,yIndex));
      TString spectraFitName(Form("finalSpectrum_%s_Cent%02d_yIndex%02d_Fit",
				  speciesName.Data(),iCentBin,yIndex));
      
      spectra[yIndex] = (TGraphErrors *)file->Get(Form("%sSpectra_%s/%s",
						       Type.Data(),speciesName.Data(),
						       spectraName.Data()));
      spectraTOF[yIndex] = (TGraphErrors *)file->Get(Form("%sSpectra_%s/%s",
							  Type.Data(),speciesName.Data(),
							  spectraNameTOF.Data()));
      spectraFit[yIndex] = (TF1 *)file->Get(Form("SpectraFits_%s/%s",
					 speciesName.Data(),
					 spectraFitName.Data()));

      cout <<spectra[yIndex] <<" " <<spectraTOF[yIndex] <<endl;
      //Skip if no spectrum was found for this rapidity bin
      if (spectra[yIndex] == NULL)
	continue;
      
      //Make sure spectra have points
      if (spectra[yIndex]->GetN() == 0){
	delete spectra[yIndex];
	continue;
      }
      if (spectraTOF[yIndex] && spectraTOF[yIndex]->GetN() == 0){
	delete spectraTOF[yIndex];
	spectraTOF[yIndex] = NULL;
      }
      
      //Remove high mTm0 Point
      TGraphChop(spectra[yIndex],maxmTm0,false);
      if (spectraTOF[yIndex])
	TGraphChop(spectraTOF[yIndex],maxmTm0,false);

      //Remove points with large errors
      RemovePointsWithLargeErrors(spectra[yIndex]);
      if (spectraTOF[yIndex])
	RemovePointsWithLargeErrors(spectraTOF[yIndex]);
      
      //Scale the Spectra
      scaledSpectra[yIndex] = TGraphScale(spectra[yIndex],pow(scaleFactor,yIndex-midRapidityIndex));
      if (spectraTOF[yIndex])
	scaledSpectraTOF[yIndex] = TGraphScale(spectraTOF[yIndex],pow(scaleFactor,yIndex-midRapidityIndex));

      //Scale the Spectra Fit
      Double_t fitMin(0), fitMax(0);
      if (corrected && spectraFit[yIndex]){
	funcToScale[yIndex] = spectraFit[yIndex];
	spectraFit[yIndex]->GetRange(fitMin,fitMax);
	scaledSpectraFit[yIndex] = new TF1(Form("%s_Scaled",spectraFit[yIndex]->GetName()),
					   scaleFunction,fitMin,maxmTm0,2);
	scaledSpectraFit[yIndex]->SetParameter(0,pow(scaleFactor,yIndex-midRapidityIndex));
	scaledSpectraFit[yIndex]->SetParameter(1,yIndex);
      }

      //Create the Label
      Double_t yLocation = scaledSpectra[yIndex]->GetY()[scaledSpectra[yIndex]->GetN()-1];
      Double_t xLocation = scaledSpectra[yIndex]->GetX()[scaledSpectra[yIndex]->GetN()-1]+.05;
      
      if (spectraTOF[yIndex]){
	yLocation = TMath::Min(yLocation,scaledSpectraTOF[yIndex]->GetY()[scaledSpectraTOF[yIndex]->GetN()-1]);
	xLocation = TMath::Max(xLocation,scaledSpectraTOF[yIndex]->GetX()[scaledSpectraTOF[yIndex]->GetN()-1]+.05);
      }

      if (corrected && scaledSpectraFit[yIndex]){
	xLocation = maxmTm0+.025;
	yLocation = scaledSpectraFit[yIndex]->Eval(xLocation);
      }
      
      label[yIndex] = new TPaveText(xLocation,yLocation,xLocation+.2,yLocation,"NB");
      label[yIndex]->SetTextSize(0.028);
      label[yIndex]->AddText(Form("y = %.1f",GetRapidityRangeCenter(yIndex)));
      
      //Set the Mid Rapidity Spectrum to Red
      if (yIndex == midRapidityIndex){
	scaledSpectra[yIndex]->SetMarkerColor(kRed);
	label[yIndex]->SetTextColor(kRed);
	if (spectraTOF[yIndex] && scaledSpectraTOF[yIndex])
	  scaledSpectraTOF[yIndex]->SetMarkerColor(kRed);
	if (corrected && scaledSpectraFit[yIndex]){
	  scaledSpectraFit[yIndex]->SetLineColor(kRed);
	  scaledSpectraFit[yIndex]->SetLineWidth(3);
	}	
      }
      else{
	scaledSpectra[yIndex]->SetMarkerColor(kBlack);
	if (spectraTOF[yIndex]) scaledSpectraTOF[yIndex]->SetMarkerColor(kBlack);
	if (corrected && scaledSpectraFit[yIndex]){
	  scaledSpectraFit[yIndex]->SetLineColor(kBlack);
	  scaledSpectraFit[yIndex]->SetLineWidth(3);
	}
      }

      if (corrected) scaledSpectraFit[yIndex]->Draw("SAME");
      scaledSpectra[yIndex]->Draw("PZ");
      if (spectraTOF[yIndex] && scaledSpectraTOF[yIndex])
	scaledSpectraTOF[yIndex]->Draw("PZ");
      label[yIndex]->Draw("SAME");
      
    }//End Loop Over Spectra
    
    if (!canvas)
      continue;
    cout <<"here" <<endl;

    //Create the Title
    TPaveText *title = new TPaveText(.56,.82,.91,.92,"NBNDCBR");
    title->SetFillColor(kWhite);
    title->SetBorderSize(0);
    title->SetTextSize(.05);
    title->SetTextAlign(32);
    title->AddText(Form("%s Spectra %s",
			particleInfo->GetParticleSymbol(speciesIndex,charge).Data(),
			system.Data()));
    title->AddText(Form("#sqrt{s_{NN}} = %.03g GeV",energy));
    /*
    title->AddText(Form("%d-%d%% Central",
			iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
			(int)GetCentralityPercents().at(iCentBin)));
    */
    title->GetLine(1)->SetTextSize(.05);
    //title->GetLine(2)->SetTextSize(.05);
    title->Draw("SAME");
    
    TMarker *midYMarker = new TMarker(0,0,kFullCircle);
    midYMarker->SetMarkerColor(kRed);
    midYMarker->SetMarkerSize(1.5);
        
    TMarker *notMidYMarker = new TMarker(0,0,kFullCircle);
    notMidYMarker->SetMarkerColor(kBlack);
    notMidYMarker->SetMarkerSize(1.5);
    
    TLegend *leg = new TLegend(.65,.76,.92,.82);
    leg->SetLineColor(kWhite);
    leg->SetBorderSize(0);
    leg->SetFillColor(kWhite);
    leg->SetTextSize(.035);
    leg->AddEntry(midYMarker,"Mid Rapidity","P");
    leg->AddEntry(notMidYMarker,Form("x %g^{#pm n}",scaleFactor),"P");
    leg->Draw("SAME");

    TPaveText *centralityTitle = new TPaveText(.2,.13,.47,.19,"BRNBNDC");
    centralityTitle->SetFillColor(kWhite);
    centralityTitle->SetBorderSize(0);
    centralityTitle->SetTextFont(63);
    centralityTitle->SetTextSize(25);
    centralityTitle->AddText(Form("%d-%d%% Central",
				  iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
				  (int)GetCentralityPercents().at(iCentBin)));
    centralityTitle->Draw("SAME");

    TPaveText *starPrelim = new TPaveText(.55,.13,.90,.19,"BRNBNDC");
    starPrelim->SetFillColor(kWhite);
    starPrelim->SetBorderSize(0);
    starPrelim->SetTextFont(63);
    starPrelim->SetTextSize(18);
    starPrelim->AddText("STAR PRELIMINARY");
    starPrelim->Draw("SAME");

    TMarker *tpcMarker = new TMarker(0,0,kFullCircle);
    tpcMarker->SetMarkerColor(kGray+2);
    tpcMarker->SetMarkerSize(1.5);

    TMarker *tofMarker = new TMarker(0,0,kFullSquare);
    tofMarker->SetMarkerColor(kGray+2);
    tofMarker->SetMarkerSize(1.5);
    
    TLegend *leg1 = new TLegend(.67,.72,.89,.76);
    leg1->SetBorderSize(0);
    leg1->SetFillColor(kWhite);
    leg1->SetTextSize(.035);
    leg1->SetNColumns(2);
    leg1->AddEntry(tpcMarker,"TPC","P");
    leg1->AddEntry(tofMarker,"TOF","P");
    leg1->Draw("SAME");

    
    if (save){
      canvas->Print(Form("%s.gif",canvas->GetName()));
    }
    
  }//End Loop Over centrality bins
  

}

