{
//=========Macro generated from canvas: canvas1/canvas1
//=========  (Tue Feb  7 00:01:12 2017) by ROOT version5.34/36

  gStyle->SetOptStat(0);
  
   TCanvas *canvas1 = new TCanvas("canvas1", "canvas1",21,46,800,600);
   canvas1->Range(0,0,1,1);
   canvas1->SetFillColor(0);
   canvas1->SetBorderMode(0);
   canvas1->SetBorderSize(2);
   canvas1->SetFrameBorderMode(0);
   canvas1->SetTicks();
   canvas1->SetRightMargin(.05);
   canvas1->SetTopMargin(.05);
   
   TH1F *yieldSpreadHisto = new TH1F("yieldSpreadHisto","",200,0,30);
   yieldSpreadHisto->SetBinContent(1,38);
   yieldSpreadHisto->SetBinContent(2,40);
   yieldSpreadHisto->SetBinContent(3,44);
   yieldSpreadHisto->SetBinContent(4,48);
   yieldSpreadHisto->SetBinContent(5,46);
   yieldSpreadHisto->SetBinContent(6,42);
   yieldSpreadHisto->SetBinContent(7,33);
   yieldSpreadHisto->SetBinContent(8,36);
   yieldSpreadHisto->SetBinContent(9,36);
   yieldSpreadHisto->SetBinContent(10,38);
   yieldSpreadHisto->SetBinContent(11,27);
   yieldSpreadHisto->SetBinContent(12,30);
   yieldSpreadHisto->SetBinContent(13,40);
   yieldSpreadHisto->SetBinContent(14,35);
   yieldSpreadHisto->SetBinContent(15,23);
   yieldSpreadHisto->SetBinContent(16,20);
   yieldSpreadHisto->SetBinContent(17,18);
   yieldSpreadHisto->SetBinContent(18,14);
   yieldSpreadHisto->SetBinContent(19,12);
   yieldSpreadHisto->SetBinContent(20,19);
   yieldSpreadHisto->SetBinContent(21,16);
   yieldSpreadHisto->SetBinContent(22,22);
   yieldSpreadHisto->SetBinContent(23,8);
   yieldSpreadHisto->SetBinContent(24,10);
   yieldSpreadHisto->SetBinContent(25,12);
   yieldSpreadHisto->SetBinContent(26,13);
   yieldSpreadHisto->SetBinContent(27,14);
   yieldSpreadHisto->SetBinContent(28,11);
   yieldSpreadHisto->SetBinContent(29,11);
   yieldSpreadHisto->SetBinContent(30,9);
   yieldSpreadHisto->SetBinContent(31,7);
   yieldSpreadHisto->SetBinContent(32,11);
   yieldSpreadHisto->SetBinContent(33,6);
   yieldSpreadHisto->SetBinContent(34,14);
   yieldSpreadHisto->SetBinContent(35,9);
   yieldSpreadHisto->SetBinContent(36,7);
   yieldSpreadHisto->SetBinContent(37,9);
   yieldSpreadHisto->SetBinContent(38,10);
   yieldSpreadHisto->SetBinContent(39,8);
   yieldSpreadHisto->SetBinContent(40,7);
   yieldSpreadHisto->SetBinContent(41,3);
   yieldSpreadHisto->SetBinContent(42,10);
   yieldSpreadHisto->SetBinContent(43,5);
   yieldSpreadHisto->SetBinContent(44,12);
   yieldSpreadHisto->SetBinContent(45,6);
   yieldSpreadHisto->SetBinContent(46,8);
   yieldSpreadHisto->SetBinContent(47,3);
   yieldSpreadHisto->SetBinContent(48,4);
   yieldSpreadHisto->SetBinContent(49,5);
   yieldSpreadHisto->SetBinContent(50,3);
   yieldSpreadHisto->SetBinContent(51,3);
   yieldSpreadHisto->SetBinContent(52,3);
   yieldSpreadHisto->SetBinContent(53,5);
   yieldSpreadHisto->SetBinContent(54,1);
   yieldSpreadHisto->SetBinContent(55,3);
   yieldSpreadHisto->SetBinContent(56,2);
   yieldSpreadHisto->SetBinContent(57,8);
   yieldSpreadHisto->SetBinContent(58,1);
   yieldSpreadHisto->SetBinContent(59,6);
   yieldSpreadHisto->SetBinContent(60,3);
   yieldSpreadHisto->SetBinContent(61,4);
   yieldSpreadHisto->SetBinContent(62,3);
   yieldSpreadHisto->SetBinContent(63,4);
   yieldSpreadHisto->SetBinContent(64,3);
   yieldSpreadHisto->SetBinContent(65,2);
   yieldSpreadHisto->SetBinContent(66,5);
   yieldSpreadHisto->SetBinContent(67,1);
   yieldSpreadHisto->SetBinContent(69,1);
   yieldSpreadHisto->SetBinContent(70,2);
   yieldSpreadHisto->SetBinContent(73,5);
   yieldSpreadHisto->SetBinContent(74,3);
   yieldSpreadHisto->SetBinContent(75,3);
   yieldSpreadHisto->SetBinContent(76,2);
   yieldSpreadHisto->SetBinContent(77,1);
   yieldSpreadHisto->SetBinContent(78,2);
   yieldSpreadHisto->SetBinContent(79,1);
   yieldSpreadHisto->SetBinContent(82,1);
   yieldSpreadHisto->SetBinContent(84,3);
   yieldSpreadHisto->SetBinContent(85,2);
   yieldSpreadHisto->SetBinContent(86,1);
   yieldSpreadHisto->SetBinContent(87,1);
   yieldSpreadHisto->SetBinContent(93,1);
   yieldSpreadHisto->SetBinContent(96,1);
   yieldSpreadHisto->SetBinContent(100,1);
   yieldSpreadHisto->SetEntries(1000);

   yieldSpreadHisto->GetXaxis()->SetRangeUser(0,20);

   gSystem->Load("../bin/ParticleInfo_cxx.so");
   ParticleInfo particleInfo;
   int pid = PION;
   int charge = -1;
   
   yieldSpreadHisto->SetMarkerStyle(kFullCircle);
   yieldSpreadHisto->SetMarkerColor(particleInfo.GetParticleColor(pid));
   yieldSpreadHisto->SetLineColor(kBlack);
   yieldSpreadHisto->SetMarkerSize(1.2);

   
   yieldSpreadHisto->GetXaxis()->SetLabelFont(42);
   yieldSpreadHisto->GetXaxis()->SetLabelSize(0.038);
   yieldSpreadHisto->GetXaxis()->SetTitleSize(25);
   yieldSpreadHisto->GetXaxis()->SetTitleFont(63);
   yieldSpreadHisto->GetXaxis()->SetTitleOffset(1.05);
   yieldSpreadHisto->GetXaxis()->SetTitle("|#it{N'}_{s} - #it{N'}_{Default}|");
   
   yieldSpreadHisto->GetYaxis()->SetLabelFont(42);
   yieldSpreadHisto->GetYaxis()->SetLabelSize(0.038);
   yieldSpreadHisto->GetYaxis()->SetTitleSize(25);
   yieldSpreadHisto->GetYaxis()->SetTitleFont(63);
   yieldSpreadHisto->GetYaxis()->SetTitleOffset(.90);
   yieldSpreadHisto->GetYaxis()->SetTitle("Counts");

   
   yieldSpreadHisto->Draw("E");
   canvas1->Modified();

   Double_t mean = yieldSpreadHisto->GetMean();
   TLine *line = new TLine(mean,0,mean,40);
   line->SetLineColor(kBlack);
   line->SetLineStyle(9);
   line->SetLineWidth(3);
   line->Draw();
   
   TPaveText *title = new TPaveText(.44,.69,.91,.91,"NDC");
   title->SetFillColor(kWhite);
   title->SetBorderSize(0);
   title->SetTextFont(63);
   title->SetTextSize(25);
   title->SetTextAlign(11);
   title->AddText("Invariant Yield Systematic Error");
   title->AddText("#sqrt{s_{NN}} = 7.7 GeV | PID: #pi^{-}");
   title->AddText("Cent=[0,5]% | y_{#pi^{-}} = [-0.05,0.05]");
   title->AddText("m_{T}-m_{#pi^{-}} = [0.100,0.125] (GeV/c^{2})");
   title->GetLine(1)->SetTextSize(20);
   title->GetLine(2)->SetTextSize(20);
   title->GetLine(3)->SetTextSize(20);
   title->Draw();

   TLegend *leg = new TLegend(.61,.58,.88,.68);
   leg->SetBorderSize(1);
   leg->SetFillColor(kWhite);
   leg->SetTextFont(63);
   leg->SetTextSize(20);
   leg->SetHeader(Form("Entries: %.05g",yieldSpreadHisto->GetEntries()));
   leg->AddEntry(line,Form("Mean: %.03g",mean),"L");
   leg->Draw();

   canvas1->Modified();
   canvas1->cd();
   canvas1->SetSelected(canvas1);
   
}
