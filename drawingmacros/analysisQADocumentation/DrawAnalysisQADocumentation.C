#include <TStyle.h>

#include <../../inc/globalDefinitions.h>

//The Other Drawing macros
#include "../corrections/DrawInterBinmTm0Study.C"
#include "../corrections/DrawMuonContamination.C"
#include "../corrections/DrawFeedDownBackground.C"
#include "../corrections/DrawEfficiencyCorrection.C"
#include "../corrections/DrawEnergyLossCorrection.C"
#include "../corrections/DrawTofMatchEfficiencyCorrection.C"
#include "../spectra/DrawSingleSpectrum.C"
#include "../yieldExtraction/DrawYieldExtractionFit.C"

bool save = true;

int PAGENUMBER=0;

//___DEFINED BELOW_________________________________
void MakeTitlePage(TCanvas *page);
void AddPageNumber(TCanvas *page);
void MakeHeader(TCanvas *page, TString system, TString config,
		double energy, int pid, int charge, int centBin,
		int yIndex);
void MakeSummaryPage(TCanvas *page, TFile *spectraFile, TFile *correctionFile,
		     double energy, int pid, int charge, int centBin, int yIndex);
int MakeYieldExtractPage(TCanvas *page, TFile *spectraFile, TFile *yieldHistoFile, TGraphErrors *tpcSpectrum,
			  TGraphErrors *tofSpectrum, double energy, TString config,
			  int pid, int charge, int centBin, int yIndex, int spectrumPointStart);

//___MAIN_________________________________________
void DrawAnalysisQADocumentation(TString spectraFileName="", TString yieldHistoFileName="", TString correctionFileName="",TString tofMatchFileName, TString system="", TString eventConfig="", double energy=0.0,int pid=0, int charge =0){

  gStyle->SetOptStat(0);
  
  //Load the necessary Libs
  gROOT->LoadMacro("../../macros/loadLibs.C");
  LoadParticleInfoModule();
  LoadUserCutsModule();
  SetVariableUserCuts(energy,eventConfig,"");
  
  ParticleInfo particleInfo;
  const int nCentralityBins(9);

  //Open all the necessary Files
  TFile *spectraFile = new TFile(spectraFileName,"READ");
  TFile *yieldHistoFile = new TFile(yieldHistoFileName,"READ");
  TFile *correctionFile = new TFile(correctionFileName,"READ");
  TFile *tofMatchingFile = new TFile(tofMatchFileName,"READ");
  
  //Create a Page on which to draw
  gStyle->SetPaperSize(TStyle::kUSLetter);
  TCanvas *page = new TCanvas("page","page",20,20,850,1150);

  //******* TITLE PAGE ********
  TPaveText *docTitle = new TPaveText(.2,.45,.7,.7,"NDC");
  docTitle->SetFillColor(kWhite);
  docTitle->SetBorderSize(0);
  docTitle->SetTextFont(63);
  docTitle->SetTextSize(40);
  docTitle->SetTextAlign(11);
  docTitle->AddText("#scale[.6]{Analysis Summary Plots}");
  docTitle->AddText(Form("System: %s",system.Data()));
  docTitle->AddText(Form("Energy: #sqrt{s_{NN}} = %.03g GeV",energy));
  docTitle->AddText(Form("Configuration: %s", eventConfig.Data()));
  docTitle->AddText(Form("Species: %s",particleInfo.GetParticleSymbol(pid,charge).Data()));
  
  docTitle->Draw();
  AddPageNumber(page);
  if (save){
    page->Print(Form("page%05d.pdf",PAGENUMBER-1));
  }
  
  page->Clear();

  //******* SPECTRA SUMMARY PAGES ********
  //Loop Over the Centrality Bins
  for (int iCentBin=nCentralityBins-1; iCentBin>=0; iCentBin--){

    //Loop Over the Rapidity Bins
    for (int yIndex=GetMinRapidityIndexOfInterest(pid);
	 yIndex<=GetMaxRapidityIndexOfInterest(pid); yIndex++){

      //Get the Raw Spectra
      TGraphErrors *tpcRaw =
	(TGraphErrors *)spectraFile->Get(Form("RawSpectra_%s/rawSpectra_%s_Cent%02d_yIndex%02d",
					      particleInfo.GetParticleName(pid,charge).Data(),
					      particleInfo.GetParticleName(pid,charge).Data(),
					      iCentBin,yIndex));
      TGraphErrors *tofRaw =
	(TGraphErrors *)spectraFile->Get(Form("RawSpectra_%s/rawSpectraTOF_%s_Cent%02d_yIndex%02d",
					      particleInfo.GetParticleName(pid,charge).Data(),
					      particleInfo.GetParticleName(pid,charge).Data(),
					      iCentBin,yIndex));
      
      if (!tpcRaw && !tofRaw){
	continue;
      }
      
      //Make the Summary Page for this centrality and rapidity bin
      MakeHeader(page,system,eventConfig,energy,pid,charge,iCentBin,yIndex);
      MakeSummaryPage(page,spectraFile,correctionFile,tofMatchingFile,
		      energy,pid,charge,iCentBin,yIndex);

      if (tpcRaw)
	delete tpcRaw;
      if (tofRaw)
	delete tofRaw;

    }//End Loop Over Rapidity Bins

  }//End Loop Over Centrality Bins

  //Combine All of the Spectra Summary Pages into a Document
  if (save){
    gSystem->Exec(Form("pdfunite page*.pdf tempStart.pdf"));
    gSystem->Exec("rm page*.pdf");
  }

  // return;
  //******* YIELD EXTRACTION PAGES ********
  //Loop Over the Centrality Bins
  for (int iCentBin=nCentralityBins-1; iCentBin>=0; iCentBin--){

    //Loop Over the Rapidity Bins
    for (int yIndex=GetMinRapidityIndexOfInterest(pid);
	 yIndex<=GetMaxRapidityIndexOfInterest(pid); yIndex++){

      //Get the Raw Spectra
      TGraphErrors *tpcRaw =
	(TGraphErrors *)spectraFile->Get(Form("RawSpectra_%s/rawSpectra_%s_Cent%02d_yIndex%02d",
					      particleInfo.GetParticleName(pid,charge).Data(),
					      particleInfo.GetParticleName(pid,charge).Data(),
					      iCentBin,yIndex));
      TGraphErrors *tofRaw =
	(TGraphErrors *)spectraFile->Get(Form("RawSpectra_%s/rawSpectraTOF_%s_Cent%02d_yIndex%02d",
					      particleInfo.GetParticleName(pid,charge).Data(),
					      particleInfo.GetParticleName(pid,charge).Data(),
					      iCentBin,yIndex));
      
      if (!tpcRaw && !tofRaw){
	continue;
      }
      
      //Make the Summary Page for this centrality and rapidity bin
      //MakeHeader(page,system,eventConfig,energy,pid,charge,iCentBin,yIndex);
      //MakeSummaryPage(page,spectraFile,correctionFile,tofMatchingFile,
      //	      energy,pid,charge,iCentBin,yIndex);

      
      int currentPoint(0);
      int totalPoints = tpcRaw->GetN();
      if (tofRaw)
	totalPoints += tofRaw->GetN();
      while (currentPoint < totalPoints){
	MakeHeader(page,system,eventConfig,energy,pid,charge,iCentBin,yIndex);
	currentPoint = MakeYieldExtractPage(page,spectraFile,yieldHistoFile,tpcRaw,tofRaw,
					    energy,eventConfig,pid,charge,iCentBin,yIndex,currentPoint);
	cout <<currentPoint <<endl;
      }
      
    }//End Loop Over Rapidity Bins

  }//End Loop Over Centrality Bins

  //Combine All of the output pages into a single document
  if (save){
    gSystem->Exec(Form("pdfunite tempStart.pdf page*.pdf AnalysisSummaryPlots_%s%02d_%s_%s.pdf",
		       system.Data(),(int)energy,
		       eventConfig.Data(),
		       particleInfo.GetParticleName(pid,charge).Data()));
    gSystem->Exec("rm page*.pdf tempStart.pdf");
  }
  
}

//_________________________________________________________
int MakeYieldExtractPage(TCanvas *page, TFile *spectraFile, TFile *yieldHistoFile, TGraphErrors *tpcSpectrum,
			  TGraphErrors *tofSpectrum, double energy, TString config,
			  int pid, int charge, int centBin, int yIndex, int spectrumPointStart){

  AddPageNumber(page);

  TPad *mainPad = new TPad("mainPad","mainPad",.0,.025,1.0,.95);
  int nPadsX(3), nPadsY(4);
  int nPads = nPadsX * nPadsY;
  mainPad->Divide(nPadsX,nPadsY);
  mainPad->Draw();

  int tpcPoints = tpcSpectrum->GetN();
  int tofPoints(0);
  if (tofSpectrum)
    tofPoints = tofSpectrum->GetN();
  int currentPoint = spectrumPointStart;
  
  
  for (int iPad = 1; iPad<=nPads; iPad++){

    int mTm0Index(0);
    bool detector;

    if (currentPoint < tpcPoints){
      mTm0Index = GetmTm0Index(tpcSpectrum->GetX()[currentPoint]);
      detector = true;
    }
    else if (currentPoint < tpcPoints + tofPoints){
      mTm0Index = GetmTm0Index(tofSpectrum->GetX()[currentPoint - tpcPoints]);
      detector = false;
    }
    else
      continue;
    
    mainPad->cd(iPad);
    DrawSingleYieldExtractionFit(spectraFile,yieldHistoFile,gPad,energy,config,pid,charge,centBin,yIndex,
				 mTm0Index,detector);

    currentPoint++;

  }
  
  mainPad->IsModified();
  mainPad->Update();
  page->IsModified();
  page->Update();
  gSystem->Sleep(10);
  
  if (save){
    page->Print(Form("page%05d.pdf",PAGENUMBER-1));
  }

  page->Clear();
  
  return spectrumPointStart += nPads;
}


//_________________________________________________________
void MakeSummaryPage(TCanvas *page, TFile *spectraFile, TFile *correctionFile,
		     TFile *tofMatchingFile, 
		     double energy, int pid, int charge, int centBin, int yIndex){

  AddPageNumber(page);

  TPad *mainPad = new TPad("mainPad","mainPad",.0,.025,1.0,.95);
  mainPad->Divide(2,4);//,0.0,0.0);
  mainPad->Draw();


  mainPad->cd(1);
  DrawSingleTofMatchingPlot(tofMatchingFile,gPad,energy,pid,charge,centBin,yIndex);

  mainPad->cd(2);
  DrawSingleInterBinmTm0Study(correctionFile,gPad,energy,pid, charge, centBin,yIndex);

  mainPad->cd(3);
  if (pid == PION){
    DrawSingleMuonContaminationPlot(correctionFile,gPad,energy,pid,charge,centBin,yIndex);
  }
  else {
    TPaveText *muonNA = new TPaveText(.2,.3,.7,.8,"brNDC");
    muonNA->SetTextFont(63);
    muonNA->SetTextSize(30);
    muonNA->SetFillColor(kWhite);
    muonNA->SetBorderSize(0);
    muonNA->AddText("#splitline{Muon Contamination}{- Not Applicable -}");
    muonNA->Draw("SAME");
  }

  mainPad->cd(4);
  if (pid == PION || pid == PROTON){
    DrawSingleFeedDownPlot(correctionFile,gPad,energy,pid,charge,centBin,yIndex);
  }
  else {
    TPaveText *feedDownNA = new TPaveText(.2,.3,.7,.8,"brNDC");
    feedDownNA->SetTextFont(63);
    feedDownNA->SetTextSize(30);
    feedDownNA->SetFillColor(kWhite);
    feedDownNA->SetBorderSize(0);
    feedDownNA->AddText("#splitline{Feed Down Correction}{- Not Applicable -}");
    feedDownNA->Draw("SAME");
  }

  mainPad->cd(5);
  DrawSingleEnergyLossPlot(correctionFile,gPad,energy,pid,charge,centBin,yIndex);

  mainPad->cd(6);
  DrawSingleEfficiencyPlot(correctionFile,gPad,energy,pid,charge,centBin,yIndex);

  mainPad->cd(7);
  DrawSingleSpectrum(spectraFile,gPad,energy,pid,charge,centBin,yIndex);

  mainPad->cd(8);
  DrawSpectrumStats(spectraFile,gPad,energy,pid,charge,centBin,yIndex);
    
  mainPad->IsModified();
  mainPad->Update();
  page->IsModified();
  page->Update();
  gSystem->Sleep(10);

  if (save){
    page->Print(Form("page%05d.pdf",PAGENUMBER-1));
  }

  page->Clear();
}

//_________________________________________________________
void MakeHeader(TCanvas *page, TString system, TString config,
		double energy, int pid, int charge, int centBin, int yIndex){

  ParticleInfo pInfo;
  
  TPaveText *header = new TPaveText(.0,.95,1.0,1.0);
  header->SetFillColor(kBlack);
  header->SetBorderSize(0);
  header->SetTextFont(63);
  header->SetTextColor(kWhite);
  header->SetTextSize(22);

  header->AddText(Form("#scale[.7]{#splitline{%s #sqrt{s_{NN}} = %.03g GeV}{%s   %s}} | Centrality=[%d,%d]% | y_{%s}=[%.3g,%.3g]",
		       system.Data(),energy,
		       config.Data(),
		       pInfo.GetParticleSymbol(pid,charge).Data(),
		       GetCentralityRangeHigh(centBin),
		       GetCentralityRangeLow(centBin),
		       pInfo.GetParticleSymbol(pid,charge).Data(),
		       GetRapidityRangeLow(yIndex),
		       GetRapidityRangeHigh(yIndex)));
  
  header->Draw();
}


//_________________________________________________________
void AddPageNumber(TCanvas *page){

  TPaveText *pageNum = new TPaveText(.95,.01,1,.025);
  pageNum->SetFillColor(kWhite);
  pageNum->SetBorderSize(0);
  pageNum->SetTextFont(63);
  pageNum->SetTextSize(15);
  pageNum->AddText(Form("%d",PAGENUMBER));
  pageNum->Draw("SAME");

  PAGENUMBER++;

}
