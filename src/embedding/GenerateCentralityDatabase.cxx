//Generates the Centrality Database from the ADC files which will be
//used to classify the centrality of events in the embedding/minimc files.

//C++ includes
#include <iostream>

//ROOT includes
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TCanvas.h>

//DataCollectorReaderLibs Sub-Module Includes
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "CentralityDatabaseClass.h"

void GenerateCentralityDatabase(TString inputDataFile, TString outputFile){

  //Get the Z-Vertex Cuts
  std::pair<double,double> zVertexCuts = GetZVertexCuts();

  //Get Radial Vertex Cuts
  std::pair<double,double> rVertexCuts = GetRadialVertexCuts();

  //Get Beam Spot Location
  std::pair<double,double> beamSpotLocation = GetBeamSpotLocation();

  //Get Allowed Triggers
  std::vector<unsigned int> allowedTriggers = GetAllowedTriggers();
  
  //Read the Input Files
  DavisDstReader *davisDst = NULL;
  if (zVertexCuts.first != -999 && zVertexCuts.second != 999)
    davisDst = new DavisDstReader(inputDataFile,zVertexCuts.first,zVertexCuts.second);
  else
    davisDst = new DavisDstReader(inputDataFile);

  //Turn Off the Track Tree since we only need event and vertex information
  davisDst->SetBranchStatus("TrackInfo",0);


  
  //Create some class pointers we will use to access the data in the tree
  PrimaryVertexInfo *primaryVertex = NULL;
  EventInfo *event = NULL;

  Long64_t nEvents = davisDst->GetEntries();

  //Create a vector of CentralityDatabase Objects. We will temporarily store
  //information in this vector and write it to the tree at the end.
  std::vector<CentralityDatabase *> centralityDatabaseVec;
  CentralityDatabase *currentCentDatabase;
  
  //Loop Over the Entries and fill the centrality database tree
  for (Long64_t iEvent=0; iEvent<nEvents; iEvent++){

    event = davisDst->GetEntry(iEvent);
    if (!IsGoodEvent(event))
      continue;

    //Check if this event has a new run number
    currentCentDatabase = NULL;
    if (IsNewRunNumber(&centralityDatabaseVec,(Long64_t)event->GetRunNumber())){
      centralityDatabaseVec.push_back(new CentralityDatabase((Long64_t)event->GetRunNumber()));
      
      currentCentDatabase = centralityDatabaseVec.back();
      currentCentDatabase->SetVzRange(zVertexCuts.first,zVertexCuts.second);
      currentCentDatabase->SetVrRange(rVertexCuts.first,rVertexCuts.second);
      currentCentDatabase->SetBeamSpot(beamSpotLocation.first,beamSpotLocation.second);
    }
    else{
      currentCentDatabase = GetRunNumberEntry(&centralityDatabaseVec,(Long64_t)event->GetRunNumber());
    }

    if (!currentCentDatabase){
      cout <<"ERROR - GenerateCentralityDatabase - currentCentDatabase pointer not set! A previous enetry with this run number was not found and it was not new. Something went wrong!\n";
      exit (EXIT_FAILURE);
    }

    //NOTE: The CentralityDatabase Variable can currently only handle one vertex per event.
    //      If in the future this needs to be update for multiple verticies the CentralityDatabase
    //      class will need to be updated as well.    
    Int_t nPrimaryVertices = event->GetNPrimaryVertices();
    for (Int_t iVertex=0; iVertex<nPrimaryVertices; iVertex++){

      primaryVertex = event->GetPrimaryVertex(iVertex);
      if (!IsGoodVertex(primaryVertex))
	continue;

      UInt_t triggerID = event->GetTriggerID(&allowedTriggers);
      Int_t centralityVariable = GetCentralityVariable(primaryVertex);
      Int_t centralityBin      = GetCentralityBin(centralityVariable,triggerID,
						  primaryVertex->GetZVertex());

      //Skip Events with bad Centrality Bins
      if (centralityBin < 0)
	continue;
      
      currentCentDatabase->AddEvent(event->GetEventNumber(),centralityBin,
				    primaryVertex->GetXVertex(),primaryVertex->GetYVertex(),
				    primaryVertex->GetZVertex());
      
    }//End Loop Over Vertices
    
  }//End Loop Over Event

  //Now Fill the Tree
  
  //Create the Centrality Database Variable
  CentralityDatabase *centDatabase = new CentralityDatabase();
  
  //Create the Output file and Tree
  TFile *outFile = new TFile(outputFile,"RECREATE");
  TTree *centralityTree = new TTree("CentralityTree","CentralityTree");
  TBranch *branch = centralityTree->Branch("Runs",&centDatabase);  

  for (Long64_t i=0; i<(Long64_t)centralityDatabaseVec.size(); i++){

    centDatabase = centralityDatabaseVec.at(i);
    if (centDatabase->GetNEvents() == 0)
      continue;

    centralityTree->Fill();
    
  }

  centralityTree->Write();
  outFile->Close();
  
}
