#include <iostream>
#include <vector>
#include <utility>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>
#include <TAxis.h>
#include <TDirectory.h>
#include <TH1F.h>
#include <TLine.h>
#include <TThread.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraClass.h"
#include "SpectraFitUtilities.h"
#include "SpectraFitFunctions.h"
#include "SimFitter.h"
#include "StyleSettings.h"

bool draw = false;
bool save = true;

struct threadArgs {
  SpectraClass *spectrum = NULL;
  TF1 *fitFunc = NULL;
  TMatrixDSym *covMatrix = NULL;
  int whichFunc;

  threadArgs(SpectraClass *s, TF1 *func, TMatrixDSym *m, int i){
    spectrum = s;
    fitFunc = func;
    covMatrix = m;
    whichFunc = i;
  }
  ~threadArgs(){}
};

void *threadKernel(void *ARGS){

  threadArgs *args = (threadArgs *)ARGS;

  args->spectrum->AddSpectraFit(args->fitFunc,NULL/*args->covMatrix*/,args->whichFunc);
  
}

//___________________________________________________________________
void fitProtonSpectra(TString spectraFileName, Int_t CHARGE, Int_t userCentBin = -999, Double_t userRapidity = -999){

  gStyle->SetOptFit(1);
  ParticleInfo *particleInfo = new ParticleInfo();
  const unsigned int nCentBins(GetNCentralityBins());
  const int pid = PROTON;

    //Set the Charge Vector
  std::vector<int> charges;
  charges.push_back(CHARGE);
  charges.push_back(-1*CHARGE);

  //Set the Pid Vector
  std::vector<int> particles;
  particles.push_back(pid);
  
  const unsigned int nCharges(charges.size());
  const unsigned int nParticles(particles.size());
  
  //Open the Spectra File
  TFile *spectraFile = new TFile(spectraFileName,save ? "UPDATE" : "READ");
  TString saveDirPlus = TString::Format("FitSpectraClass_%s",particleInfo->GetParticleName(pid,charges.at(0)).Data());
  TString saveDirFitPlus = TString::Format("%s_FitFuncs",saveDirPlus.Data());
  TString saveDirMinus = TString::Format("FitSpectraClass_%s",particleInfo->GetParticleName(pid,charges.at(1)).Data());
  TString saveDirFitMinus = TString::Format("%s_FitFuncs",saveDirMinus.Data());

  //Create a canvas if drawing
  TCanvas *canvas = NULL;
  TH1F *spectraFrame = NULL;
  TCanvas *dNdyCanvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    canvas->SetTopMargin(.05);
    canvas->SetRightMargin(.05);
    gPad->SetLogy();
   
    dNdyCanvas = new TCanvas("parCanvas","parCanvas",20,700,500*nParticles,1000);
    dNdyCanvas->Divide(nParticles,2);
    dNdyCanvas->cd(1);
    gPad->DrawFrame(-1.5,0,1.5,80);
    dNdyCanvas->cd(2);
    gPad->DrawFrame(-1.5,0,1.5,10);
  }

  //Create the Fit Function that will be copied to each spectrum's individual fit
  TF1 *nominalFit = new TF1("nominalFit",BlastWaveModelFit,0.001,2.0,6);
  TF1 *sysFit = new TF1("sysFit",SiemensRasmussen,0.001,2.0,4);
  nominalFit->SetNpx(500);
  sysFit->SetNpx(500);
  //MARKER OF START CHANGES
  //Parameter Enumerations for the Simultaneous Fit
  int nNonAmpPars(6);
  enum simFitPars {TEMP,SURFACEVELOCITY_PROTONPLUS,SURFACEVELOCITY_PROTONMINUS,TRANSVELOCITYPOWER,
		   RADIALINTEGRALLIMIT,PMASS,AMP};

  int nNonAmpParsSys(3);
  enum sysFitPars {TEMPSYS,BETASYS_PROTONPLUS,PMASSSYS,AMPSYS};

  //Create the dNdy Graphs and Chi2 Graphs
  std::vector<std::vector<TGraphErrors *> >dNdyGraphs
    (nCentBins, std::vector<TGraphErrors *>
     (nParticles,(TGraphErrors *)NULL));
  std::vector<std::vector<TGraphErrors *> >chi2Graphs
    (nCentBins, std::vector<TGraphErrors *>
     (2,(TGraphErrors *) NULL)); //Nominal / Systematic
  std::vector<std::vector<std::vector< TGraphErrors *> > > parameterGraphs
    (nCentBins, std::vector<std::vector<TGraphErrors *> >
     (nParticles, std::vector<TGraphErrors *>
      (PMASS, (TGraphErrors *) NULL)));
  for (unsigned int iCentBin = 0; iCentBin<nCentBins; iCentBin++){
    chi2Graphs.at(iCentBin).at(0) = new TGraphErrors();
    chi2Graphs.at(iCentBin).at(0)->SetName(Form("Chi2Graph_%s_Cent%02d",
						particleInfo->GetParticleName(pid).Data(),
						iCentBin));
    chi2Graphs.at(iCentBin).at(0)->SetMarkerStyle(kFullCircle);
    chi2Graphs.at(iCentBin).at(0)->SetMarkerColor(GetCentralityColor(iCentBin));
    
    chi2Graphs.at(iCentBin).at(1) = new TGraphErrors();
    chi2Graphs.at(iCentBin).at(1)->SetName(Form("Chi2Graph_%s_Cent%02d_Sys",
						particleInfo->GetParticleName(pid).Data(),
						iCentBin));
    chi2Graphs.at(iCentBin).at(1)->SetMarkerStyle(kOpenCircle);
    chi2Graphs.at(iCentBin).at(1)->SetMarkerColor(GetCentralityColor(iCentBin));
    
    for (unsigned int i=0; i<nParticles; i++){
      dNdyGraphs.at(iCentBin).at(i) = new TGraphErrors();
      dNdyGraphs.at(iCentBin).at(i)->SetMarkerStyle(kFullCircle);
      dNdyGraphs.at(iCentBin).at(i)->SetMarkerColor(GetCentralityColor(iCentBin));
      for (unsigned int j=0; j<parameterGraphs.at(iCentBin).at(i).size(); j++){
	parameterGraphs.at(iCentBin).at(i).at(j) = new TGraphErrors();
	parameterGraphs.at(iCentBin).at(i).at(j)->SetName(Form("FitPar%d_%s_Cent%02d",
							       j,
							       particleInfo->GetParticleName(pid).Data(),
							       iCentBin));
	parameterGraphs.at(iCentBin).at(i).at(j)->SetMarkerStyle(kFullCircle);
	parameterGraphs.at(iCentBin).at(i).at(j)->SetMarkerColor(GetCentralityColor(iCentBin));
      }
    }
  }

  //Load the Spectra
  //The most internal vector holds all the spectra of equal |y|
  std::vector<std::vector<std::vector<SpectraClass *> > > spectra
    (nCentBins,std::vector<std::vector<SpectraClass *> >
     (nRapidityBins, std::vector<SpectraClass *>
      (0,(SpectraClass *)NULL)));
  std::vector<std::vector<std::vector<TGraphErrors *> > > spectraToFit
    (nCentBins,std::vector<std::vector<TGraphErrors *> >
     (nRapidityBins, std::vector<TGraphErrors *>
      (0,(TGraphErrors *)NULL)));
  std::vector<std::vector<std::vector<TF1 *> > > fitFuncs 
    (nCentBins,std::vector<std::vector<TF1 *> >
     (nRapidityBins, std::vector<TF1 *>
      (0,(TF1 *)NULL)));
  std::vector<std::vector<std::vector<TF1 *> > > sysFuncs 
    (nCentBins,std::vector<std::vector<TF1 *> >
     (nRapidityBins, std::vector<TF1 *>
      (0,(TF1 *)NULL)));
  for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    for (unsigned int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=nRapidityBins/2; yIndex++){

      std::vector<int> yIndexVec;
      yIndexVec.push_back(yIndex);
      if ((int)yIndex != GetRapidityIndex(-1.*GetRapidityRangeCenter(yIndex)))
	yIndexVec.push_back(GetRapidityIndex(-1.*GetRapidityRangeCenter(yIndex)));
      
      
      for (unsigned int iY=0; iY<yIndexVec.size(); iY++){
	for (unsigned int iCharge=0; iCharge<nCharges; iCharge++){
	  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
	    int charge = charges.at(iCharge);
	    TString spectraName = TString::Format("CorrectedSpectra_%s_Cent%02d_yIndex%02d_Total",
						  particleInfo->GetParticleName(pid,charge).Data(),
						  iCentBin,yIndexVec.at(iY));
	    TString spectraClassDir = TString::Format("SpectraClass_%s",
						      particleInfo->GetParticleName(pid,charge).Data());
	    TString spectrumDir = TString::Format("CorrectedSpectra_%s",
						  particleInfo->GetParticleName(pid,charge).Data());
	    TString spectrumFitDir = TString::Format("%s_FitFuncs",
						     spectrumDir.Data());
	    TString dNdyDir = TString::Format("dNdyGraph_%s",
					      particleInfo->GetParticleName(pid,charge).Data());
	    spectraFile->cd();
	    spectraFile->cd(spectraClassDir.Data());
	    SpectraClass *temp = (SpectraClass *)gDirectory->Get(spectraName.Data());
	    
	    if (temp){

	      spectra.at(iCentBin).at(yIndex).push_back(temp);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraFile(spectraFile);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraClassDir(spectraClassDir);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraDir(spectrumDir);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraNamePrefix("correctedSpectra");
	      spectra.at(iCentBin).at(yIndex).back()->LoadSpectra();
	      
	      spectraToFit.at(iCentBin).at(yIndex).push_back(spectra.at(iCentBin).at(yIndex).back()->GetTGraph());

	      fitFuncs.at(iCentBin).at(yIndex).push_back(new TF1(*nominalFit));
	      sysFuncs.at(iCentBin).at(yIndex).push_back(new TF1(*sysFit));
	      sysFuncs.at(iCentBin).at(yIndex).back()->SetLineStyle(9);

	      fitFuncs.at(iCentBin).at(yIndex).back()->SetName(Form("%s_Fit0",spectraName.Data()));
	      sysFuncs.at(iCentBin).at(yIndex).back()->SetName(Form("%s_Fit1",spectraName.Data()));
	    }//End If Spectra Exists
	    
	  }//End Loop Over Particles
	}//End Loop Over Charges
      }//End Loop Over Mirrored RapidityIndices
    }//End Loop Over yIndex
  }//End Loop Over CentIndex	  


  //NOW FOR THE FITS
  for (int iCentBin=nCentBins-1; iCentBin>=0; iCentBin--){
    
    std::vector<double> prevFitParams;
    std::vector<double> prevFitParamsSys;

    //Skip Bins that are not requested by the user
    if (userCentBin != -999 && userCentBin >= 0 && (int)iCentBin != userCentBin){
      cout <<Form("Skipping centrality bin %02d by user request.",iCentBin) <<endl;
      continue;
    }

    double midRapidityBetaPlus(0);
    double midRapidityBetaMinus(0);
    double midRapidityBetaSys(0);
    
    for (int yIndex=(nRapidityBins/2); yIndex>=0; yIndex--){

      //Skip Bins that are not requested by the user
      if (userRapidity != -999 && fabs(userRapidity-GetRapidityRangeCenter(yIndex)) > rapidityBinWidth/2.0){
	cout <<Form("Skipping rapidity bin %02d by user request.",yIndex) <<endl;
	continue;
      }

      //Get the Rapidity of This bin
      Double_t rapidity = fabs(GetRapidityRangeCenter(yIndex));
      
      //Skip this bin if there are no spectra
      const unsigned int nSpectra = spectraToFit.at(iCentBin).at(yIndex).size();
      if (nSpectra == 0)
	continue;

      //Skip this bin if none of the spectra have enough points
      bool skipDueToLackOfPoints = false;
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	if (spectraToFit.at(iCentBin).at(yIndex).at(iSpectrum)->GetN() < 5)
	  skipDueToLackOfPoints = true;
	else{
	  skipDueToLackOfPoints = false;
	  break;
	}	  
      }
      if (skipDueToLackOfPoints){
	cout <<"Skipping bin since no spectrum has enough points" <<endl;
	continue;
      }

      //*************************
      //***** NOMINAL FIT *******
      //*************************
      //Compute the Total Number of Parameters in the Sim Fit
      const int nTotalPars = nNonAmpPars + nSpectra;

      //Define the Parameter Relationships
      std::vector<std::vector<int> > parRelations(nSpectra,std::vector<int> (0,-1));
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){

	parRelations.at(iSpectrum).resize(fitFuncs.at(iCentBin).at(yIndex).at(iSpectrum)->GetNpar(),0);
	parRelations.at(iSpectrum).at(0) = AMP+iSpectrum;
	parRelations.at(iSpectrum).at(1) = TEMP;
	
	if (spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetCharge() > 0)
	  parRelations.at(iSpectrum).at(2) = SURFACEVELOCITY_PROTONPLUS;
	else
	  parRelations.at(iSpectrum).at(2) = SURFACEVELOCITY_PROTONMINUS;
	
	parRelations.at(iSpectrum).at(3) = TRANSVELOCITYPOWER;
	parRelations.at(iSpectrum).at(4) = RADIALINTEGRALLIMIT;
	parRelations.at(iSpectrum).at(5) = PMASS;
      }

      //Seed the Parameters of the Total Fit
      std::vector<double> totalFitPars(nTotalPars);
      
      //If there are shape parameters from a previous fit use them
      if (prevFitParams.size() > 0){

	//First the shape parameters 
	for (int iPar=0; iPar<nNonAmpPars; iPar++){
	  totalFitPars.at(iPar) = prevFitParams.at(iPar);
	}

      }
      //And if not
      else {
	totalFitPars.at(TEMP) = .12;
	totalFitPars.at(SURFACEVELOCITY_PROTONPLUS) = .64;
	totalFitPars.at(SURFACEVELOCITY_PROTONMINUS) = .64;
	totalFitPars.at(TRANSVELOCITYPOWER) = 0.5;
	totalFitPars.at(RADIALINTEGRALLIMIT) = 1;
	totalFitPars.at(PMASS) = particleInfo->GetParticleMass(pid);
      }

      //The amplidute parameters
      for (int iPar=nNonAmpPars; iPar<nTotalPars; iPar++){
	if (nSpectra == prevFitParams.size() - nNonAmpPars){
	  totalFitPars.at(iPar) = prevFitParams.at(iPar);
	}
	else 
	  totalFitPars.at(iPar) = 50000;
      }
      
      //Create the Sim Fitter and Configure the Parameters
      SimFitter simFitter(spectraToFit.at(iCentBin).at(yIndex),fitFuncs.at(iCentBin).at(yIndex));
      simFitter.Init(totalFitPars);
      simFitter.SetPrintLevel(1);
      simFitter.SetParameterRelationships(parRelations);

      //Fix the Mass and Radial Integral Limit Parameters which are both always fixed
      simFitter.FixParameter(PMASS);
      simFitter.FixParameter(RADIALINTEGRALLIMIT);
      simFitter.FixParameter(TRANSVELOCITYPOWER);

      //Fix or Limit the Parameters
      simFitter.SetParLimits(SURFACEVELOCITY_PROTONPLUS,0.1,.98);
      simFitter.SetParLimits(SURFACEVELOCITY_PROTONMINUS,0.1,.98);
      if (rapidity > rapidityBinWidth/2){ //Not Mid Rapidity
	simFitter.FixParameter(TEMP);
	simFitter.SetParLimits(SURFACEVELOCITY_PROTONPLUS,
			       TMath::Max(.01,midRapidityBetaPlus - .1),
			       TMath::Min(.98,midRapidityBetaPlus + .1));
	simFitter.SetParLimits(SURFACEVELOCITY_PROTONMINUS,
			       TMath::Max(.01,midRapidityBetaMinus -.1),
			       TMath::Min(.98,midRapidityBetaPlus + .1));
      }
      else {
	simFitter.SetParLimits(TEMP,.1,.3);
      }
      
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	if (nSpectra == prevFitParams.size()-nNonAmpPars){
	  simFitter.SetParLimits(AMP+iSpectrum,prevFitParams.at(AMP+iSpectrum)*.8,
				 prevFitParams.at(AMP+iSpectrum)*1.3);
	}
	else 
	  simFitter.SetParLimits(AMP+iSpectrum,10,100000000);
      }

      //Do the Fit
      ROOT::Fit::FitResult fitResults = simFitter.Fit();

      int status = fitResults.Status();
      int nTrials(0);
      while (status > 1 && nTrials < 10){
	cout <<"Refitting: " <<iCentBin <<" " <<yIndex <<endl;

	
	
	fitResults = simFitter.Fit();

	status = fitResults.Status();
	nTrials++;
      }
      
      std::vector<TF1 *> fitResultFuncs = simFitter.GetFitFunctions();

      if (rapidity < rapidityBinWidth/2.0){
	midRapidityBetaPlus = fitResults.Parameter(SURFACEVELOCITY_PROTONPLUS);
	midRapidityBetaMinus = fitResults.Parameter(SURFACEVELOCITY_PROTONMINUS);
      }

      //Transfer the the fit parameters to the previous vector and into the parameter graphs
      if (prevFitParams.size() > 0)
	prevFitParams.clear();
      prevFitParams.resize(nTotalPars);
      for (unsigned int iPar=0; iPar<prevFitParams.size(); iPar++){
	prevFitParams.at(iPar) = fitResults.Parameter(iPar);
      }
      for (unsigned int iPar=TEMP; iPar<PMASS; iPar++){
	parameterGraphs.at(iCentBin).at(0).at(iPar)->
	  SetPoint(parameterGraphs.at(iCentBin).at(0).at(iPar)->GetN(),
		   rapidity,fitResults.Parameter(iPar));
	parameterGraphs.at(iCentBin).at(0).at(iPar)->
	  SetPointError(parameterGraphs.at(iCentBin).at(0).at(iPar)->GetN()-1,
			rapidityBinWidth/2.0,fitResults.ParError(iPar));
      }
      
      //Set the Fit Name
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	fitResultFuncs.at(iSpectrum)->SetName(fitFuncs.at(iCentBin).at(yIndex).at(iSpectrum)->GetName());
	fitResultFuncs.at(iSpectrum)->SetChisquare(fitResults.Chi2());
	fitResultFuncs.at(iSpectrum)->SetNDF(fitResults.Ndf());
      }

      //Create Threads to Add the Fit to the Spectrum (Error Calculation takes along time)
      std::vector<TThread *> threadsVec(nSpectra);
      std::vector<threadArgs *> threadArgsVec(nSpectra);
      for (unsigned int iThread=0; iThread<threadsVec.size(); iThread++){
	threadArgsVec.at(iThread) = new threadArgs(spectra.at(iCentBin).at(yIndex).at(iThread),
						   fitResultFuncs.at(iThread),
						   simFitter.GetCovarianceMatrix(iThread),
						   0);
	threadsVec.at(iThread) = new TThread(Form("%02d",iThread),
					     threadKernel,(void *)threadArgsVec.at(iThread));
	threadsVec.at(iThread)->Run();
      }

      //Join the Threads
      for (unsigned int iThread=0; iThread<threadsVec.size(); iThread++){
	threadsVec.at(iThread)->Join();
	threadsVec.at(iThread)->Delete();
      }

      //Add the Chi2 of the fit to the graph
      chi2Graphs.at(iCentBin).at(0)->SetPoint(chi2Graphs.at(iCentBin).at(0)->GetN(),
					      rapidity,fitResults.Chi2()/(float)fitResults.Ndf());
      
      //Move the dNdy To the Graphs
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	int index(0);
	dNdyGraphs.at(iCentBin).at(index)->
	  SetPoint(dNdyGraphs.at(iCentBin).at(index)->GetN(),
		   GetRapidityRangeCenter(spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetRapidityIndex()),
		   spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdy(0));
	dNdyGraphs.at(iCentBin).at(index)->
	  SetPointError(dNdyGraphs.at(iCentBin).at(index)->GetN()-1,
			rapidityBinWidth/2.0,
			spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdyErr(0));
      }

      
      //***************************
      //****** SYSTEMATIC FIT *****
      //***************************
      //Compute the Total Number of Parameters in the Sys Fit
      const int nTotalParsSys = nNonAmpParsSys + nSpectra;

      //Define the Parameter Relationships
      std::vector<std::vector<int> > parRelationsSys(nSpectra,std::vector<int> (0,-1));
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	parRelationsSys.at(iSpectrum).resize(sysFuncs.at(iCentBin).at(yIndex).at(iSpectrum)->GetNpar(),0);
	parRelationsSys.at(iSpectrum).at(0) = AMPSYS+iSpectrum;
	parRelationsSys.at(iSpectrum).at(1) = TEMPSYS;
	parRelationsSys.at(iSpectrum).at(2) = BETASYS_PROTONPLUS;
	/*
	if (spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetCharge() > 0)
	  parRelationsSys.at(iSpectrum).at(2) = BETASYS_PROTONPLUS;
	else
	  parRelationsSys.at(iSpectrum).at(2) = BETASYS_PROTONMINUS;
	*/	
	parRelationsSys.at(iSpectrum).at(3) = PMASSSYS;
      }

      //Seed the Parmeters of the Total Sys Fit
      std::vector<double> totalFitParsSys(nTotalParsSys);

      //If there are shape parameters from the previous fit then use them
      if (prevFitParamsSys.size() > 0){

	for (int iPar=0; iPar<nNonAmpParsSys; iPar++){
	  totalFitParsSys.at(iPar) = prevFitParamsSys.at(iPar);
	}
	
      }

      //And if Not
      else {
	totalFitParsSys.at(TEMPSYS) = .12;
	totalFitParsSys.at(BETASYS_PROTONPLUS) = .1;
	//totalFitParsSys.at(BETASYS_PROTONMINUS) = .7;
	totalFitParsSys.at(PMASSSYS) = particleInfo->GetParticleMass(pid);
      }

      //The Amplitude Parameters
      for (int iPar=nNonAmpParsSys; iPar<nTotalParsSys; iPar++){
	totalFitParsSys.at(iPar) = 300000;
      }

      //Create the Sim Fitter and Configure the Parameters
      SimFitter sysFitter(spectraToFit.at(iCentBin).at(yIndex),sysFuncs.at(iCentBin).at(yIndex));
      sysFitter.Init(totalFitParsSys);
      sysFitter.SetPrintLevel(1);
      sysFitter.SetParameterRelationships(parRelationsSys);

      //Fix the Mass
      sysFitter.FixParameter(PMASSSYS);

      //Set Parameter Limits
      sysFitter.SetParLimits(TEMPSYS,.08,.3);      
      //sysFitter.SetParLimits(BETASYS_PROTONMINUS,0.5,1);
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	sysFitter.SetParLimits(AMPSYS+iSpectrum,100,100000000);
      }

      cout <<rapidity <<" " <<rapidityBinWidth/2.0 <<" " <<midRapidityBetaSys <<" " <<endl;
      if (rapidity > rapidityBinWidth/2.0){ //Not Mid Rapidity
	sysFitter.SetParLimits(BETASYS_PROTONPLUS,
			       TMath::Min(.001,midRapidityBetaSys - .1),
			       TMath::Max(0.9,midRapidityBetaSys + .1));
      }
      else {
	sysFitter.SetParLimits(BETASYS_PROTONPLUS,0.001,.9);
      }

      //Do the Fit
      cout <<endl <<"SYSTEMATIC FIT";//<<endl;
      ROOT::Fit::FitResult fitResultsSys = sysFitter.Fit();

      if (rapidity < rapidityBinWidth/2.0){
	midRapidityBetaSys = fitResultsSys.Parameter(BETASYS_PROTONPLUS);
      }

      int fitStatus = fitResultsSys.Status();
      double chi2NDF = fitResultsSys.Chi2() / (double)fitResultsSys.Ndf();
      int nIters(0);

      cout <<"FitStatus: " <<fitStatus <<endl;
      /*
      if ((fitStatus != 0 || chi2NDF > 10) && nIters < 10){
	cout <<"Refitting!" <<endl;
	fitResultsSys = sysFitter.Fit();
	fitStatus = fitResultsSys.Status();
	chi2NDF = fitResultsSys.Chi2() / (double)fitResultsSys.Ndf();
	nIters++;
      }
      */
      std::vector<TF1 *> fitResultFuncsSys = sysFitter.GetFitFunctions();
 
      //Transfer the Fit Parameters to the PrevVector
      if (prevFitParamsSys.size() > 0)
	prevFitParamsSys.clear();
      prevFitParamsSys.resize(nTotalParsSys);
      for (unsigned int iPar=0; iPar<prevFitParamsSys.size(); iPar++){
	prevFitParamsSys.at(iPar) = fitResultsSys.Parameter(iPar);
      }
 
      //Add the Fit to the Spectrum
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	fitResultFuncsSys.at(iSpectrum)->SetName(sysFuncs.at(iCentBin).at(yIndex).at(iSpectrum)->GetName());
	fitResultFuncsSys.at(iSpectrum)->SetChisquare(fitResultsSys.Chi2());
	fitResultFuncsSys.at(iSpectrum)->SetNDF(fitResultsSys.Ndf());
	spectra.at(iCentBin).at(yIndex).at(iSpectrum)->AddSpectraFit(fitResultFuncsSys.at(iSpectrum),
								     NULL
								     /*sysFitter.GetCovarianceMatrix(iSpectrum)*/,1);
      }
      cout <<"Adding Chi2" <<endl;
      //Add the Chi2 of the fit to the graph
      chi2Graphs.at(iCentBin).at(1)->SetPoint(chi2Graphs.at(iCentBin).at(1)->GetN(),
					      rapidity,fitResultsSys.Chi2()/(float)fitResultsSys.Ndf());

      //Move the dNdy To the Graphs
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	int index(0);
	dNdyGraphs.at(iCentBin).at(index)->
	  SetPoint(dNdyGraphs.at(iCentBin).at(index)->GetN(),
		   GetRapidityRangeCenter(spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetRapidityIndex()),
		   spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdy(1));
	dNdyGraphs.at(iCentBin).at(index)->
	  SetPointError(dNdyGraphs.at(iCentBin).at(index)->GetN()-1,
			rapidityBinWidth/2.0,
			spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdyErr(1));
      }
      cout <<"Done with Systematics" <<endl;

      //************************
      //**** DRAW IF DESIRED ****
      //************************
      if (draw){
	canvas->cd();
	spectraFrame = canvas->DrawFrame(0.0,.0001,2,500);
	
	//Loop Over the Spectra Involved in the Simulatneous Fit
	for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	  canvas->cd();
	  spectraToFit.at(iCentBin).at(yIndex).at(iSpectrum)->Draw("PZ");
	  fitResultFuncs.at(iSpectrum)->Draw("SAME");
	  fitResultFuncsSys.at(iSpectrum)->Draw("SAME");

	  //Draw the dNdy Distributions
	  dNdyCanvas->cd(1);
	  dNdyGraphs.at(iCentBin).at(0)->Draw("PZ");
	  dNdyCanvas->cd(2);
	  chi2Graphs.at(iCentBin).at(0)->Draw("PZ");
	  chi2Graphs.at(iCentBin).at(1)->Draw("PZ");
	}//End Loop Over Spectra
	
	canvas->Update();
	dNdyCanvas->Update();
	gSystem->Sleep(100);	

      }//End Draw

      
    }//End Loop Over Rapidity Bins
    
  }//End Loop Over Centrality Bins
  
  //************************
  //**** SAVE IF DESIRED ***
  //************************
  if (save){

    spectraFile->cd();
    spectraFile->mkdir(saveDirPlus.Data());
    spectraFile->mkdir(saveDirFitPlus.Data());
    spectraFile->mkdir(saveDirMinus.Data());
    spectraFile->mkdir(saveDirFitMinus.Data());
    spectraFile->mkdir(Form("FitSpectraClass_%s_FitFuncs_FitPars",
			    particleInfo->GetParticleName(pid).Data()));

    
    for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){

      //Skip Bins that are not requested by the user
      if (userCentBin != -999 && userCentBin >= 0 && (int)iCentBin != userCentBin){
	cout <<Form("Skipping centrality bin %02d by user request.",iCentBin) <<endl;
	continue;
      }
      
      for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

	//Skip Bins that are not requested by the user
	if (userRapidity != -999 && fabs(userRapidity-GetRapidityRangeCenter(yIndex)) > rapidityBinWidth/2.0){
	  cout <<Form("Skipping rapidity bin %02d by user request.",yIndex) <<endl;
	  continue;
	}
		
	for (unsigned int i=0; i<spectra.at(iCentBin).at(yIndex).size(); i++){
	  
	  spectra.at(iCentBin).at(yIndex).at(i)->SetSpectraFile(spectraFile);

	  if (spectra.at(iCentBin).at(yIndex).at(i)->GetCharge() > 0){
	    spectra.at(iCentBin).at(yIndex).at(i)->SetSpectraClassDir(saveDirPlus);
	    spectra.at(iCentBin).at(yIndex).at(i)->SetSpectraFitDir(saveDirFitPlus);
	  }
	  else {
	    spectra.at(iCentBin).at(yIndex).at(i)->SetSpectraClassDir(saveDirMinus);
	    spectra.at(iCentBin).at(yIndex).at(i)->SetSpectraFitDir(saveDirFitMinus);
	  }
	  
	  //Save the Spectra Class and Fit Funcs, but do not overwrite the spectra themselves.
	  spectra.at(iCentBin).at(yIndex).at(i)->WriteSpectrum(false,true);
	  
	}//End Loop Over Spectra
      }//End Loop Over Rapidity Bins

      spectraFile->cd();
      spectraFile->cd(Form("FitSpectraClass_%s_FitFuncs_FitPars",
			   particleInfo->GetParticleName(pid).Data()));
      chi2Graphs.at(iCentBin).at(0)->Write("",TObject::kOverwrite);
      chi2Graphs.at(iCentBin).at(1)->Write("",TObject::kOverwrite);
      for (unsigned int iPar=TEMP; iPar<PMASS; iPar++){
	parameterGraphs.at(iCentBin).at(0).at(iPar)->Write("",TObject::kOverwrite);
      }
      
    }//End Loop Over Centrality Bins

    cout <<"Spectra Saved!" <<endl;
  }//End SAVE
  cout <<"Fit Proton Spectra Finished Successfully!" <<endl;
}
