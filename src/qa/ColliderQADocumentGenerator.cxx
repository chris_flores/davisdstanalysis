//Quality Assurace Report Generator

#include <iostream>
#include <vector>
#include <cmath>
#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TFile.h>
#include <TPad.h>
#include <TSystem.h>
#include <TPaveText.h>
#include <TLatex.h>
#include <TMarker.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TString.h>
#include <TStyle.h>
#include <TFile.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

//Globals
Bool_t save = true;
Bool_t fullQA = true;
TString name;
TString outDir;
TCanvas *pageCanvas;
TPaveText *titleBar;
TPaveText *pageNum;
std::vector<std::vector<TObject *> > object;
std::vector<std::vector<TString> > objectOpts;
std::vector<TPad *> pad(6,(TPad *)NULL);
TString padOpts[6];
TLegend *globalLegend;
Double_t leftMargin(.05);
Double_t topMargin(.88);
Int_t nPage(0);

//Functions Defined Below Main
void GeneratePage(TString pageTitle="", TLegend *leg = NULL);
TH1D *GetZProjectionHisto1D (TH3 *h3, Double_t xMin, Double_t xMax,
			     Double_t yMin, Double_t yMax,TString nameSuffix="", TString titleBase="");
TH1D *GetXProjectionHisto1D (TH3 *h3, Double_t yMin, Double_t yMax,
			     Double_t zMin, Double_t zMax,TString nameSuffix, TString titleBase);
TH2D *GetZProjectionHisto2D (TH3 *h3, Double_t xMin, Double_t xMax,
			     Double_t yMin, Double_t yMax,TString nameSuffix, TString titleBase);
void Normalize(TH1D *h);
		  

//___MAIN______________________________________________________________
void ColliderQADocumentGenerator(TString embeddingFile, TString dataQAFile, TString outDirectory, Int_t pidInterest, Int_t charge){

  //Set the Necessary Constants
  ParticleInfo *particleInfo = new ParticleInfo();
  TString particleType = particleInfo->GetParticleName(pidInterest,charge);
  double yRangeMin = rapidityMin;
  double yRangeMax = rapidityMax;
  double yBinWidth = rapidityBinWidth;
  double minVz     = GetZVertexCuts().first;
  double maxVz     = GetZVertexCuts().second;
  double minVr     = GetRadialVertexCuts().first;
  double maxVr     = GetRadialVertexCuts().second;
  name     = GetEventConfiguration();
  outDir   = outDirectory;
  const int npTBins = 5;

  //Open Files
  TFile *embFile = new TFile(embeddingFile,"READ");
  TFile *dataFile    = new TFile(dataQAFile,"READ");
  
  //Output File
  TString outDocument = TString::Format("%s/%s%d_%s.pdf",
					outDir.Data(),
					name.Data(),(int)GetCollisionEnergy(),
					particleInfo->GetParticleName(pidInterest,charge).Data());
  
  //Title
  TString title = TString::Format("%s #sqrt{s_{NN}} = %g GeV %s",
				  name.Data(),GetCollisionEnergy(),
				  particleInfo->GetParticleSymbol(pidInterest,charge).Data());


  
  //Create a Canvas and Array of Pads
  gStyle->SetPaperSize(TStyle::kUSLetter);
  pageCanvas = new TCanvas("pageCanvas","pageCanvas",600,20,850*2,1100*2);

  //Get the Summary Vector
  //TVectorD *summaryVector = (TVectorD *)embFile->Get("SummaryVector");

  object.resize(6,std::vector<TObject *>(0,(TObject*)NULL));
  objectOpts.resize(6,std::vector<TString >(6,""));
  
  titleBar = new TPaveText(0,.95,1,1);
  titleBar->SetFillColor(kBlack);
  titleBar->AddText(title);
  titleBar->SetTextColor(kWhite);
  titleBar->Draw();

  pageNum = new TPaveText(.94,.01,.99,.06);
  pageNum->SetFillColor(kWhite);
  pageNum->SetBorderSize(0);
  pageNum->SetTextSize(.02);

  TMarker *embMarker = new TMarker(0,0,kFullSquare);
  TMarker *matchMarker = new TMarker(0,0,kFullSquare);
  TMarker *dataMarker = new TMarker(0,0,kFullSquare);

  embMarker->SetMarkerColor(kBlack);
  matchMarker->SetMarkerColor(kRed);
  dataMarker->SetMarkerColor(kBlue);
  
  globalLegend = new TLegend(.25,.25,.8,.8);
  globalLegend->SetFillColor(kWhite);
  globalLegend->SetBorderSize(0);
  globalLegend->AddEntry(embMarker,"Embedded Tracks","PL");
  globalLegend->AddEntry(matchMarker,"Matched Tracks","PL");
  globalLegend->AddEntry(dataMarker,"#diamond Primary Tracks","PL");
    
  //*****************************************************************
  //     Title Page
  //*****************************************************************
  TPaveText *superTitle = new TPaveText(leftMargin,.75,1-leftMargin,.85);
  superTitle->SetFillColor(kWhite);
  superTitle->SetBorderSize(0);
  superTitle->AddText("Track Quality and Embedding Report");
  superTitle->AddText(title);
  superTitle->Draw();

  //Embedding Request Parameters
  TPaveText *embRequest = new TPaveText(leftMargin,.5,.5,.7);
  embRequest->SetFillColor(kWhite);
  embRequest->SetBorderSize(0);
  embRequest->SetTextSize(.02);
  embRequest->SetTextAlign(11);
  embRequest->AddText("Embedding Request Parameters");
  embRequest->GetLine(0)->SetTextSize(.025);
  embRequest->AddLine(leftMargin,.9,.95,.9);
  embRequest->AddText(Form("Library: %s",GetStarLibraryVersion().Data()));
  embRequest->Draw();

  //Embedding Statistics
  int nEmbEvents =  1;
  int nUsedEvents = 1;
  
  TPaveText *embStats = new TPaveText(.5,.5,1-leftMargin,.7);
  embStats->SetFillColor(kWhite);
  embStats->SetBorderSize(0);
  embStats->SetTextSize(.02);
  embStats->SetTextAlign(11);
  embStats->AddText("Embedding Statistics");
  embStats->GetLine(0)->SetTextSize(.025);
  embStats->AddLine(leftMargin,.9,.95,.9);
  embStats->AddText(Form("N_{Events,Emb} = %d",nEmbEvents));
  embStats->AddText(Form("N_{Events,Good} = %d",nUsedEvents));
  embStats->AddText(Form("N_{Good}/N_{Emb} = %g",nUsedEvents/(Float_t)nEmbEvents));
  embStats->AddText(Form("%s",""));
  embStats->AddText(Form("%s",""));
  embStats->AddText(Form("%s",""));
  embStats->Draw();

  //Event Cuts (Must Satisfy)
  TPaveText *eventCuts = new TPaveText(leftMargin,.25,.5,.4);
  eventCuts->SetFillColor(kWhite);
  eventCuts->SetBorderSize(0);
  eventCuts->SetTextAlign(11);
  eventCuts->SetTextSize(.02);
  eventCuts->AddText("Event Cuts (Must Satisfy)");
  eventCuts->GetLine(0)->SetTextSize(.025);
  eventCuts->AddLine(leftMargin,.81,.95,.81);
  eventCuts->AddText(Form("nPrimaryTracks > %d",2));
  eventCuts->AddText(Form("V_{Z} = [%g,%g] cm",minVz,maxVz));
  eventCuts->AddText(Form("V_{R} = [%g,%g] cm",minVr,maxVr));
  eventCuts->AddText("Centrality: Determined from data.");

  eventCuts->Draw();

  //Track Cuts (Must Satisfy)
  TPaveText *trackCuts = new TPaveText(.5,.15,1-leftMargin,.4);
  trackCuts->SetFillColor(kWhite);
  trackCuts->SetBorderSize(0);
  trackCuts->SetTextAlign(11);
  trackCuts->SetTextSize(.02);
  trackCuts->AddText("Track Cuts (Must Satisfy)");
  trackCuts->GetLine(0)->SetTextSize(.025);
  trackCuts->AddLine(leftMargin,.92,.95,.92);
  trackCuts->AddText(Form("Flag = [%d,%d]",0,1000));
  trackCuts->AddText(Form("nHitsFit > %d",15));
  trackCuts->AddText(Form("nHitsdEdx > %d",10));
  trackCuts->AddText(Form("nHitsFit/nHitsPoss > %g",0.52));
  trackCuts->AddText(Form("GlobalDCA < %g cm",1.0));
  trackCuts->AddText(Form("#diamond |n#sigma_{EmbSpecies}| < %d",2));
  trackCuts->AddText(Form("#diamond Charge = Charge(%s)",
			  particleInfo->GetParticleSymbol(pidInterest,charge).Data()));
  trackCuts->AddText("#diamond Additional Cuts made to data for more ");
  trackCuts->AddText("direct comparison to embedding.");
  trackCuts->Draw();

  
  pageNum->AddText(Form("%d",nPage));
  pageNum->Draw();

  pageCanvas->Update();
  if (save){
    pageCanvas->Print(Form("%s/%s_Page_%03d.pdf",outDir.Data(),name.Data(),nPage),"pdf");
    //gSystem->Exec(Form("epstopdf Page_%03d.eps && rm Page_%03d.eps",nPage,nPage));
    //gSystem->Exec(Form("convert -compress lossless Page_%03d.eps Page_%03d.pdf && rm Page_%03d.eps",nPage,nPage,nPage));
  }
  gSystem->Sleep(500);
  pageCanvas->Clear();
  
  //*****************************************************************
  //     Event Selection Page
  //*****************************************************************
  
  object[0].push_back(embFile->Get("EventQAPlots/embZVertexHisto"));
  object[1].push_back(dataFile->Get(Form("%s/%s_zVertexPostAllCuts",
					 name.Data(),name.Data(),particleType.Data(),
					 name.Data())));
  object[2].push_back(embFile->Get("EventQAPlots/embXYVertexHisto"));
  object[3].push_back(dataFile->Get(Form("%s/%s_xyVertexPostAllCuts",
					 name.Data(),name.Data(),particleType.Data(),
					 name.Data())));

  objectOpts[2][0] = "COL";
  objectOpts[3][0] = "COL";

  ((TH2D *)object[2][0])->GetXaxis()->SetRangeUser(-3,3);
  ((TH2D *)object[2][0])->GetYaxis()->SetRangeUser(-3,3);
  ((TH2D *)object[3][0])->GetXaxis()->SetRangeUser(-3,3);
  ((TH2D *)object[3][0])->GetYaxis()->SetRangeUser(-3,3);
  
  GeneratePage("Event Selection (Left: Embedding, Right: Data)");
  //gSystem->Sleep(2000);
  
  //*****************************************************************
  //     Embeded Particle
  //*****************************************************************
  object[0].push_back(embFile->Get("EmbeddedTrackQAPlots/embTrackRapidityHisto"));
  object[1].push_back(embFile->Get("EmbeddedTrackQAPlots/embTrackmTm0Histo"));
  object[2].push_back(embFile->Get("EmbeddedTrackQAPlots/embTrackEtaPhiHisto"));
  object[3].push_back(embFile->Get("EmbeddedTrackQAPlots/embTrackPtHisto"));
  object[4].push_back(embFile->Get("EmbeddedTrackQAPlots/embTrackRapidityPtHisto"));
  object[5].push_back(embFile->Get("EventQAPlots/nEmbTrackHisto"));

  objectOpts[2][0] = "COL";
  objectOpts[3][0] = "";
  objectOpts[4][0] = "COL";

  padOpts[5] = "LOGY";

  ((TH1D *)object[3][0])->GetYaxis()->SetRangeUser(0,((TH1D *)object[3][0])->GetMaximum()*1.25);

  GeneratePage("Embedded Particle QA");

  //*****************************************************************
  //     Matched Pairs
  //*****************************************************************
  object[0].push_back(embFile->Get("MatchedTrackQAPlots/matchTrackRapidityHisto"));
  object[1].push_back(embFile->Get("MatchedTrackQAPlots/matchTrackmTm0Histo"));
  object[2].push_back(embFile->Get("MatchedTrackQAPlots/matchTrackEtaPhiHisto"));
  object[3].push_back(embFile->Get("MatchedTrackQAPlots/matchTrackPtHisto"));
  object[4].push_back(embFile->Get("MatchedTrackQAPlots/matchTrackRapidityPtHisto"));

  objectOpts[2][0] = "COL";
  objectOpts[4][0] = "COL";
  
  ((TH1D *)object[3][0])->GetYaxis()->SetRangeUser(0,((TH1D *)object[3][0])->GetMaximum()*1.25);

  GeneratePage("Matched Pairs QA");

  //*****************************************************************
  //     Detector Performance: Overview
  //*****************************************************************
  

  object[0].push_back(embFile->Get("MatchedTrackQAPlots/matchTrackEtaPhiHisto"));
  object[1].push_back(dataFile->Get(Form("%s/%s_etaphiPostCuts",
					 name.Data(),name.Data())));
  object[3].push_back(dataFile->Get(Form("%s/%s_%s/%s_etaphiPostCuts",
					 name.Data(),name.Data(),particleType.Data(),
					 name.Data())));

  ((TH2D *)object[1][0])->SetTitle(Form("All Tracks %s",((TH2D *)object[1][0])->GetTitle()));
    
  objectOpts[0][0] = "COL";
  objectOpts[1][0] = "COL";
  objectOpts[3][0] = "COL";

  GeneratePage("Detector Performance Overview: (Left - Embedding, Right - Data)");
  
  //*****************************************************************
  //     Detector Performance: Phi Projection (y,pT)
  //*****************************************************************
  TH3D *embRapidityPtPhi = (TH3D *)embFile->Get("EmbeddedTrackQAPlots/embTrackRapidityPtPhiHisto");
  TH3D *matchRapidityPtPhi = (TH3D*)embFile->Get("MatchedTrackQAPlots/matchTrackRapidityPtPhiHisto");
  TH3D *primaryRapidityPtPhi = (TH3D*)dataFile->Get(Form("%s/%s_%s/%s_ypTPhiPostCuts",
							 name.Data(),name.Data(),particleType.Data(),
							 name.Data(),particleType.Data()));
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){

    

    //Loop Over the pT Bins
    for (int pTIndex=0; pTIndex<5; pTIndex++){

      object[pTIndex].push_back(GetZProjectionHisto1D (embRapidityPtPhi,
						       yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
						       pTIndex*.4, (pTIndex+1)*.4,TString::Format("%d",pTIndex),"#phi Projection"));
      object[pTIndex].push_back(GetZProjectionHisto1D (matchRapidityPtPhi,
						       yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
						       pTIndex*.4, (pTIndex+1)*.4,TString::Format("%d",pTIndex),"#phi Projection"));
      object[pTIndex].push_back(GetZProjectionHisto1D (primaryRapidityPtPhi,
						       yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
						       pTIndex*.4, (pTIndex+1)*.4,TString::Format("%d",pTIndex),"#phi Projection"));
      ((TH1D *)object[pTIndex][2])->Rebin(4);

      if(((TH1D *)object[pTIndex][0])->GetEntries() != 0)
	Normalize((TH1D *)(object[pTIndex][0]));
      if(((TH1D *)object[pTIndex][1])->GetEntries() != 0)
	Normalize((TH1D *)(object[pTIndex][1]));
      if(((TH1D *)object[pTIndex][2])->GetEntries() != 0)
	Normalize((TH1D *)(object[pTIndex][2]));

      ((TH1D *)object[pTIndex][0])->GetYaxis()->SetRangeUser(0,
							     TMath::Max(((TH1D *)object[pTIndex][0])->GetMaximum(),
									TMath::Max(((TH1D *)object[pTIndex][1])->GetMaximum(),
										   ((TH1D *)object[pTIndex][2])->GetMaximum()))*1.25);
      
      ((TH1D *)object[pTIndex][1])->SetMarkerColor(kRed);
      ((TH1D *)object[pTIndex][1])->SetLineColor(kRed);
      ((TH1D *)object[pTIndex][2])->SetMarkerColor(kBlue);
      ((TH1D *)object[pTIndex][2])->SetLineColor(kBlue);      
      objectOpts[pTIndex][1] = "SAME";
      objectOpts[pTIndex][2] = "SAME";
    }//End Loop Over pT Bins

    GeneratePage(Form("Detector Performance: #phi Projection: y=%g",yRangeMin+yIndex*yBinWidth),globalLegend);

  } //End Loop Over Rapidity Bins

  if (fullQA == false){
    if (save){
      //gSystem->Exec(Form("pdfunite Page*.pdf %s && rm Page*.pdf",outDocument.Data()));
      //gSystem->Exec(Form("convert -compress lossless Page*.pdf %s && rm Page*.pdf",outDocument.Data()));
      gSystem->Exec(Form("gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=%s Page*.pdf",outDocument.Data()));
    }
    return;
  }
  
  //*****************************************************************
  //     Detector Performance: Rapidity Projection (pT)
  //*****************************************************************
  for (int pTIndex=0; pTIndex<5; pTIndex++){

    object[pTIndex].push_back(GetXProjectionHisto1D (embRapidityPtPhi,
						     pTIndex*.4,(pTIndex+1)*.4,-3.14,3.14,
						     TString::Format("%d",pTIndex),"Rapidity Projection"));
    object[pTIndex].push_back(GetXProjectionHisto1D (matchRapidityPtPhi,
						     pTIndex*.4,(pTIndex+1)*.4,-3.14,3.14,
						     TString::Format("%d",pTIndex),"Rapidity Projection"));
    object[pTIndex].push_back(GetXProjectionHisto1D (primaryRapidityPtPhi,
						     pTIndex*.4,(pTIndex+1)*.4,-3.14,3.14,
						     TString::Format("%d",pTIndex),"Rapidity Projection"));

    Normalize((TH1D *)(object[pTIndex][0]));
    Normalize((TH1D *)(object[pTIndex][1]));
    Normalize((TH1D *)(object[pTIndex][2]));

    ((TH1D *)object[pTIndex][0])->GetYaxis()->SetRangeUser(0,
							   TMath::Max(((TH1D *)object[pTIndex][0])->GetMaximum(),
								      TMath::Max(((TH1D *)object[pTIndex][1])->GetMaximum(),
										 ((TH1D *)object[pTIndex][2])->GetMaximum()))*1.25);
      
    ((TH1D*)object[pTIndex][1])->SetMarkerColor(kRed);
    ((TH1D*)object[pTIndex][1])->SetLineColor(kRed);
    ((TH1D*)object[pTIndex][2])->SetMarkerColor(kBlue);
    ((TH1D*)object[pTIndex][2])->SetLineColor(kBlue);    
    objectOpts[pTIndex][1] = "SAME";
    objectOpts[pTIndex][2] = "SAME";
    
  }

  GeneratePage("Detector Performance: Rapidity Projection",globalLegend);

  //*****************************************************************
  //     Detector Performance: Eta Projection (pT)
  //*****************************************************************
  TH3D *embEtaPtPhi = (TH3D*)embFile->Get("EmbeddedTrackQAPlots/embTrackEtaPtPhiHisto");
  TH3D *matchEtaPtPhi = (TH3D*)embFile->Get("MatchedTrackQAPlots/matchTrackEtaPtPhiHisto");
  TH3D *primaryEtaPtPhi = (TH3D*)dataFile->Get(Form("%s/%s_%s/%s_etapTPhiPostCuts",
						    name.Data(),name.Data(),particleType.Data(),
						    name.Data()));
  for (int pTIndex=0; pTIndex<5; pTIndex++){

    object[pTIndex].push_back(GetXProjectionHisto1D (embEtaPtPhi,
						     pTIndex*.4,(pTIndex+1)*.4,-3.14,3.14,
						     TString::Format("%d",pTIndex),"#eta Projection"));
    object[pTIndex].push_back(GetXProjectionHisto1D (matchEtaPtPhi,
						     pTIndex*.4,(pTIndex+1)*.4,-3.14,3.14,
						     TString::Format("%d",pTIndex),"#eta Projection"));
    object[pTIndex].push_back(GetXProjectionHisto1D (primaryEtaPtPhi,
						     pTIndex*.4,(pTIndex+1)*.4,-3.14,3.14,
						     TString::Format("%d",pTIndex),"#eta Projection"));

    Normalize((TH1D *)(object[pTIndex][0]));
    Normalize((TH1D *)(object[pTIndex][1]));
    Normalize((TH1D *)(object[pTIndex][2]));
    
    ((TH1D *)object[pTIndex][0])->GetYaxis()->SetRangeUser(0,
							   TMath::Max(((TH1D *)object[pTIndex][0])->GetMaximum(),
								      TMath::Max(((TH1D *)object[pTIndex][1])->GetMaximum(),
										 ((TH1D *)object[pTIndex][2])->GetMaximum()))*1.25);

    ((TH1D*)object[pTIndex][1])->SetMarkerColor(kRed);
    ((TH1D*)object[pTIndex][1])->SetLineColor(kRed);
    ((TH1D*)object[pTIndex][2])->SetMarkerColor(kBlue);
    ((TH1D*)object[pTIndex][2])->SetLineColor(kBlue);
    objectOpts[pTIndex][1] = "SAME";
    objectOpts[pTIndex][2] = "SAME";
    
  }

  GeneratePage("Detector Performance: #eta Projection",globalLegend);

  //*****************************************************************
  //     Match Track Kinematics (Emb Vs. Reco) (Full Eta Range)
  //*****************************************************************
  TH3D *matchPembPreco = (TH3D *)embFile->Get("MatchedTrackQAPlots/matchTrackEtaPembPrecoHisto");
  TH3D *matchPtembPtreco = (TH3D *)embFile->Get("MatchedTrackQAPlots/matchTrackEtaPtembPtrecoHisto");
  TH3D *matchYembYreco = (TH3D *)embFile->Get("MatchedTrackQAPlots/matchTrackEtaRapidityEmbRapidityRecoHisto");
  object[0].push_back(GetZProjectionHisto2D(matchPembPreco,
					   yRangeMin,yRangeMax,0,2.0,
					   TString::Format("%s",""),"|p^{Emb}| Vs. |p^{Reco}|"));
  object[1].push_back(GetZProjectionHisto2D(matchPtembPtreco,
					    yRangeMin,yRangeMax,0,2.0,
					    TString::Format("%s",""),"p_{T}^{Emb} Vs. p_{T}^{Reco}"));
  object[2].push_back(GetZProjectionHisto2D(matchYembYreco,
					    yRangeMin,yRangeMax,yRangeMin,yRangeMax,
					    TString::Format("%s",""),"y^{Emb}:y^{Reco}"));

  objectOpts[0][0] = "COL";
  objectOpts[1][0] = "COL";
  objectOpts[2][0] = "COL";
  
  GeneratePage("Match Track Kinematics: Emb Vs. Reco (Full #eta)");
  
  //*****************************************************************
  //     Matched Track Embeded P Vs Reconstructed P (in Eta bins)
  //*****************************************************************
  int objIndex=0;
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){

    object[objIndex].push_back(GetZProjectionHisto2D(matchPembPreco,
						     yRangeMin+yIndex*yBinWidth,yRangeMin+yIndex*yBinWidth,0,2.0,
						     TString::Format("%d",yIndex),"|p^{Emb}| Vs. |p^{Reco}|"));

    objectOpts[objIndex][0] = "COL";

    if ((objIndex>1 && objIndex%5 == 0) || yIndex == nRapidityBins-1){
      GeneratePage("Matched Track |p^{Emb}| Vs. |p^{Reco}|");
      objIndex = 0;
    }
    else
      objIndex++;    

  }
  
  //*****************************************************************
  //     Embeded Pt Vs Reconstructed Pt (in y bins)
  //*****************************************************************
  objIndex=0;
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){

    object[objIndex].push_back(GetZProjectionHisto2D(matchPtembPtreco,
						     yRangeMin+yIndex*yBinWidth,yRangeMin+yIndex*yBinWidth,0,2.0,
						     TString::Format("%d",yIndex),"p_{T}^{Emb} Vs. p_{T}^{Reco}"));

    objectOpts[objIndex][0] = "COL";

    if ((objIndex>1 && objIndex%5 == 0) || yIndex == nRapidityBins-1){
      GeneratePage("Matched Track p_{T}^{Emb} Vs. p_{T}^{Reco}");
      objIndex = 0;
    }
    else
      objIndex++;    

  }

  //*****************************************************************
  //     Embeded y Vs Reconstructed y (in Eta)
  //*****************************************************************
  objIndex=0;
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){
    
    object[objIndex].push_back(GetZProjectionHisto2D(matchYembYreco,
						     yRangeMin+yIndex*yBinWidth,yRangeMin+yIndex*yBinWidth,
						     yRangeMin,yRangeMax,
						     TString::Format("%d",yIndex),"y^{Emb} Vs. y^{Reco}"));
    
    objectOpts[objIndex][0] = "COL";
    
    if ((objIndex>1 && objIndex%5 == 0) || yIndex == nRapidityBins-1){
      GeneratePage("Matched Track y^{Emb} Vs. y^{Reco}");
      objIndex = 0;
    }
    else
      objIndex++;    
    
  }

  //*****************************************************************
  //     Embeded <nHits> Vs Matched <nHits> (in pT bins)
  //*****************************************************************
  TH3D *matchNHits = (TH3D *)embFile->Get("MatchedTrackQAPlots/matchTrackRapidityPtNHitsHisto");
  TH3D *primaryNHits = (TH3D *)dataFile->Get(Form("%s/%s_%s/%s_ynHitPointsPostCuts",
						  name.Data(),name.Data(),particleType.Data(),
						  name.Data()));

  TGraphErrors *matchNHitsGraph[npTBins];
  TGraphErrors *primaryNHitsGraph[npTBins];
  
  for (int pTIndex=0; pTIndex<npTBins; pTIndex++){

    matchNHitsGraph[pTIndex] = new TGraphErrors();
    matchNHitsGraph[pTIndex]->SetTitle(Form("<nHits> Vs Rapidity, pT = [%g,%g] GeV;y;<nHits>",pTIndex*.4,(pTIndex+1)*.4));
    primaryNHitsGraph[pTIndex] = new TGraphErrors();

    //Loop for NHits
    for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){
       
      TH1D *hMatch = GetZProjectionHisto1D(matchNHits,
					   yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					   pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu NHits");
      TH1D *hPrimary = GetZProjectionHisto1D(primaryNHits,
					     yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					     pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu NHits");

      matchNHitsGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hMatch->GetMean());
      matchNHitsGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hMatch->GetRMS()/sqrt(hMatch->GetEntries()));

      primaryNHitsGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hPrimary->GetMean());
      primaryNHitsGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hPrimary->GetRMS()/sqrt(hPrimary->GetEntries()));

      hMatch->Delete();
      hPrimary->Delete();
      
    }//End Loop Over yIndex

    object[pTIndex].push_back(matchNHitsGraph[pTIndex]);
    object[pTIndex].push_back(primaryNHitsGraph[pTIndex]); 
    
    //((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMinimum(0);
    //((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMaximum(50);
    ((TGraphErrors *)object[pTIndex][0])->SetMarkerColor(kRed);
    ((TGraphErrors *)object[pTIndex][0])->SetLineColor(kRed);
    ((TGraphErrors *)object[pTIndex][1])->SetMarkerColor(kBlue);
    ((TGraphErrors *)object[pTIndex][1])->SetLineColor(kBlue);

    objectOpts[pTIndex][0] = "AP";
    objectOpts[pTIndex][1] = "P";

  }//End Loop Over pTIndex

  GeneratePage("Average NHits",globalLegend);
  
  //*****************************************************************
  //    Embedded nHits Vs. Matched nHits (in yBins and pT bins)
  //*****************************************************************
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){
    
    for (int pTIndex=0; pTIndex<npTBins; pTIndex++){
            
      object[pTIndex].push_back(GetZProjectionHisto1D(matchNHits,
							      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
							      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"NHits"));
      object[pTIndex].push_back(GetZProjectionHisto1D(primaryNHits,
							      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
							      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"NHits"));

      Normalize((TH1D*)object[pTIndex][0]);
      Normalize((TH1D*)object[pTIndex][1]);

      ((TH1D *)object[pTIndex][0])->GetYaxis()->SetRangeUser(0,TMath::Max(
									  ((TH1D *)object[pTIndex][0])->GetMaximum(),
									  ((TH1D *)object[pTIndex][1])->GetMaximum())*1.25);
      
      ((TH1D *)object[pTIndex][0])->SetMarkerColor(kRed);
      ((TH1D *)object[pTIndex][0])->SetLineColor(kRed);
      ((TH1D *)object[pTIndex][1])->SetMarkerColor(kBlue);
      ((TH1D *)object[pTIndex][1])->SetLineColor(kBlue);
      
      objectOpts[pTIndex][0] = "";
      objectOpts[pTIndex][1] = "SAME";
      
    }//End Loop Over pTIndex

    GeneratePage(Form("nHits Distributions y=%.02g",yRangeMin+yIndex*yBinWidth),globalLegend);
    
  }//End Loop Over yIndex

  //*****************************************************************
  //    <nPossHits>  vs y (pT bins)
  //*****************************************************************
  TH3D *matchNHitsPoss = (TH3D *)embFile->Get("MatchedTrackQAPlots/matchTrackRapidityPtNHitsPossHisto");
  TH3D *primaryNHitsPoss = (TH3D *)dataFile->Get(Form("%s/%s_%s/%s_ynPossPointsPostCuts",
						  name.Data(),name.Data(),particleType.Data(),
						  name.Data()));
  TGraphErrors *matchNHitsPossGraph[npTBins];
  TGraphErrors *primaryNHitsPossGraph[npTBins];
  cout <<"Poss: " <<matchNHitsPoss <<"  " <<primaryNHitsPoss <<endl;
  
  for (int pTIndex=0; pTIndex<npTBins; pTIndex++){
    
    matchNHitsPossGraph[pTIndex] = new TGraphErrors();
    matchNHitsPossGraph[pTIndex]->SetTitle(Form("<nHitsPoss> Vs Rapidity, pT = [%g,%g] GeV;y;<nHitsPoss>",pTIndex*.4,(pTIndex+1)*.4));
    primaryNHitsPossGraph[pTIndex] = new TGraphErrors();

    //Loop For NHits Poss
    for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){
      
      TH1D *hMatch = GetZProjectionHisto1D(matchNHitsPoss,
					   yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					   pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu NHitsPoss");
      TH1D *hPrimary = GetZProjectionHisto1D(primaryNHitsPoss,
					     yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					     pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu NHitsPoss");
      
      matchNHitsPossGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hMatch->GetMean());
      matchNHitsPossGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hMatch->GetRMS()/sqrt(hMatch->GetEntries()));
      
      primaryNHitsPossGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hPrimary->GetMean());
      primaryNHitsPossGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hPrimary->GetRMS()/sqrt(hPrimary->GetEntries()));
      
      hMatch->Delete();
      hPrimary->Delete();
      
    }//End Loop Over yIndex
    
    object[pTIndex].push_back(matchNHitsPossGraph[pTIndex]);
    object[pTIndex].push_back(primaryNHitsPossGraph[pTIndex]);

    ((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMinimum(0);
    ((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMaximum(50);
    ((TGraphErrors *)object[pTIndex][0])->SetMarkerColor(kRed);
    ((TGraphErrors *)object[pTIndex][0])->SetLineColor(kRed);
    ((TGraphErrors *)object[pTIndex][1])->SetMarkerColor(kBlue);
    ((TGraphErrors *)object[pTIndex][1])->SetLineColor(kBlue);

    objectOpts[pTIndex][0] = "AP";
    objectOpts[pTIndex][1] = "P";
  
  }//End Loop Over pTIndex
  GeneratePage("Average NHitsPoss",globalLegend);
  
  //*****************************************************************
  //    nPossHits   (in y and pT bins)
  //*****************************************************************
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){
    
    for (int pTIndex=0; pTIndex<npTBins; pTIndex++){

      object[pTIndex].push_back(GetZProjectionHisto1D(matchNHitsPoss,
						      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
						      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"NHitsPoss"));
      object[pTIndex].push_back(GetZProjectionHisto1D(primaryNHitsPoss,
						      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
						      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"NHitsPoss"));

      Normalize((TH1D*)object[pTIndex][0]);
      Normalize((TH1D*)object[pTIndex][1]);

      ((TH1D *)object[pTIndex][0])->GetYaxis()->SetRangeUser(0,TMath::Max(
									  ((TH1D *)object[pTIndex][0])->GetMaximum(),
									  ((TH1D *)object[pTIndex][1])->GetMaximum())*1.25);

      ((TH1D *)object[pTIndex][0])->SetMarkerColor(kRed);
      ((TH1D *)object[pTIndex][0])->SetLineColor(kRed);
      ((TH1D *)object[pTIndex][1])->SetMarkerColor(kBlue);
      ((TH1D *)object[pTIndex][1])->SetLineColor(kBlue);
      
      objectOpts[pTIndex][0] = "";
      objectOpts[pTIndex][1] = "SAME";
      
    }//End Loop Over pTIndex

    GeneratePage(Form("nHitsPoss Distributions y=%.02g",yRangeMin+yIndex*yBinWidth),globalLegend);
  }//End Loop Over yIndex
  

  //*****************************************************************
  //    <nFitHits>  vs y (pT bins)
  //*****************************************************************
  TH3D *matchNHitsFit = (TH3D *)embFile->Get("MatchedTrackQAPlots/matchTrackRapidityPtNHitsFitHisto");
  TH3D *primaryNHitsFit = (TH3D *)dataFile->Get(Form("%s/%s_%s/%s_ynFitPointsPostCuts",
						  name.Data(),name.Data(),particleType.Data(),
						  name.Data()));
  TGraphErrors *matchNHitsFitGraph[npTBins];
  TGraphErrors *primaryNHitsFitGraph[npTBins];
  
  for (int pTIndex=0; pTIndex<npTBins; pTIndex++){
    
    matchNHitsFitGraph[pTIndex] = new TGraphErrors();
    matchNHitsFitGraph[pTIndex]->SetTitle(Form("<nHitsFit> Vs Rapidity, pT = [%g,%g] GeV;y;<nHitsFit>",pTIndex*.4,(pTIndex+1)*.4));
    primaryNHitsFitGraph[pTIndex] = new TGraphErrors();
    
    
    //Loop for NHits Fit
    for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){
      
      TH1D *hMatch = GetZProjectionHisto1D(matchNHitsFit,
					   yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					   pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu NHitsFit");
      TH1D *hPrimary = GetZProjectionHisto1D(primaryNHitsFit,
					     yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					     pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu NHitsFit");
      
      matchNHitsFitGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hMatch->GetMean());
      matchNHitsFitGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hMatch->GetRMS()/sqrt(hMatch->GetEntries()));
      
      primaryNHitsFitGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hPrimary->GetMean());
      primaryNHitsFitGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hPrimary->GetRMS()/sqrt(hPrimary->GetEntries()));
      
      hMatch->Delete();
      hPrimary->Delete();
      
    }//End Loop Over yIndex
    
    object[pTIndex].push_back(matchNHitsFitGraph[pTIndex]);
    object[pTIndex].push_back(primaryNHitsFitGraph[pTIndex]);
        
    ((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMinimum(0);
    ((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMaximum(50);
    ((TGraphErrors *)object[pTIndex][0])->SetMarkerColor(kRed);
    ((TGraphErrors *)object[pTIndex][0])->SetLineColor(kRed);
    ((TGraphErrors *)object[pTIndex][1])->SetMarkerColor(kBlue);
    ((TGraphErrors *)object[pTIndex][1])->SetLineColor(kBlue);
    
    objectOpts[pTIndex][0] = "AP";
    objectOpts[pTIndex][1] = "P";    
    
  }//End Loop Over pTIndex

  GeneratePage("Average NHitsFit",globalLegend);
        
  //*****************************************************************
  //    nFitHits   (in y and pT bins)
  //*****************************************************************
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){
    
    for (int pTIndex=0; pTIndex<npTBins; pTIndex++){
            
      object[pTIndex].push_back(GetZProjectionHisto1D(matchNHitsFit,
							      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
							      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"NHitsFit"));
      object[pTIndex].push_back(GetZProjectionHisto1D(primaryNHitsFit,
							      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
							      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"NHitsFit"));

      Normalize((TH1D*)object[pTIndex][0]);
      Normalize((TH1D*)object[pTIndex][1]);

      ((TH1D *)object[pTIndex][0])->GetYaxis()->SetRangeUser(0,TMath::Max(
									  ((TH1D *)object[pTIndex][0])->GetMaximum(),
									  ((TH1D *)object[pTIndex][1])->GetMaximum())*1.25);
      
      ((TH1D *)object[pTIndex][0])->SetMarkerColor(kRed);
      ((TH1D *)object[pTIndex][0])->SetLineColor(kRed);
      ((TH1D *)object[pTIndex][1])->SetMarkerColor(kBlue);
      ((TH1D *)object[pTIndex][1])->SetLineColor(kBlue);
      
      objectOpts[pTIndex][0] = "";
      objectOpts[pTIndex][1] = "SAME";
            
    }//End Loop Over pTIndex

    GeneratePage(Form("nHitsFit Distributions y=%.02g",yRangeMin+yIndex*yBinWidth),globalLegend);
    
  }//End Loop Over yIndex

  
  //*****************************************************************
  //    <nHitsFrac>  vs y (pT bins)
  //*****************************************************************
  TH3D *matchNHitsFrac = (TH3D *)embFile->Get("MatchedTrackQAPlots/matchTrackRapidityPtNHitsFracHisto");
  TH3D *primaryNHitsFrac = (TH3D *)dataFile->Get(Form("%s/%s_%s/%s_ynHitsFracPostCuts",
						      name.Data(),name.Data(),particleType.Data(),
						      name.Data()));
  cout <<"NHits Frac " <<matchNHitsFrac <<" " <<primaryNHitsFrac <<endl;
  TGraphErrors *matchNHitsFracGraph[npTBins];
  TGraphErrors *primaryNHitsFracGraph[npTBins];

  
  for (int pTIndex=0; pTIndex<npTBins; pTIndex++){

    matchNHitsFracGraph[pTIndex] = new TGraphErrors();
    matchNHitsFracGraph[pTIndex]->SetTitle(Form("<#frac{nHitsFit}{nHitsPoss}> Vs Rapidity, pT = [%g,%g] GeV;y;<#frac{nHitsFit}{nHitsPoss}>",pTIndex*.4,(pTIndex+1)*.4));
    primaryNHitsFracGraph[pTIndex] = new TGraphErrors();
    
    for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){

      TH1D *hMatch = GetZProjectionHisto1D(matchNHitsFrac,
					   yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					   pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu NHitsFrac");
      TH1D *hPrimary = GetZProjectionHisto1D(primaryNHitsFrac,
					     yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					     pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu NHitsFrac");

      matchNHitsFracGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hMatch->GetMean());
      matchNHitsFracGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hMatch->GetRMS()/sqrt(hMatch->GetEntries()));

      primaryNHitsFracGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hPrimary->GetMean());
      primaryNHitsFracGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hPrimary->GetRMS()/sqrt(hPrimary->GetEntries()));

      hMatch->Delete();
      hPrimary->Delete();
      
    }//End Loop Over yIndex

    object[pTIndex].push_back(matchNHitsFracGraph[pTIndex]);
    object[pTIndex].push_back(primaryNHitsFracGraph[pTIndex]);

    ((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMinimum(0);
    ((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMaximum(1.2);
    ((TGraphErrors *)object[pTIndex][0])->SetMarkerColor(kRed);
    ((TGraphErrors *)object[pTIndex][0])->SetLineColor(kRed);
    ((TGraphErrors *)object[pTIndex][1])->SetMarkerColor(kBlue);
    ((TGraphErrors *)object[pTIndex][1])->SetLineColor(kBlue);
    
    objectOpts[pTIndex][0] = "AP";
    objectOpts[pTIndex][1] = "P";
    
  }//End Loop Over pTIndex
  
  GeneratePage("Average nHitsFrac = nHitsFit/nHitsPoss",globalLegend);
  
  //*****************************************************************
  //    nHitsFrac   (in y and pT bins)
  //*****************************************************************
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){
    
    for (int pTIndex=0; pTIndex<npTBins; pTIndex++){
            
      object[pTIndex].push_back(GetZProjectionHisto1D(matchNHitsFrac,
							      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
							      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"NHitsFrac"));
      object[pTIndex].push_back(GetZProjectionHisto1D(primaryNHitsFrac,
							      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
							      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"NHitsFrac"));

      Normalize((TH1D*)object[pTIndex][0]);
      Normalize((TH1D*)object[pTIndex][1]);

      ((TH1D *)object[pTIndex][0])->GetYaxis()->SetRangeUser(0,TMath::Max(
									  ((TH1D *)object[pTIndex][0])->GetMaximum(),
									  ((TH1D *)object[pTIndex][1])->GetMaximum())*1.25);
      
      ((TH1D *)object[pTIndex][0])->SetMarkerColor(kRed);
      ((TH1D *)object[pTIndex][0])->SetLineColor(kRed);
      ((TH1D *)object[pTIndex][1])->SetMarkerColor(kBlue);
      ((TH1D *)object[pTIndex][1])->SetLineColor(kBlue);
      
      objectOpts[pTIndex][0] = "";
      objectOpts[pTIndex][1] = "SAME";
            
    }//End Loop Over pTIndex

    GeneratePage(Form("nHitsFrac Distributions y=%.02g",yRangeMin+yIndex*yBinWidth),globalLegend);
    
  }//End Loop Over yIndex
  
  
  //*****************************************************************
  //    <ndEdxHits>  vs y (pT bins)
  //*****************************************************************
  TH3D *matchNHitsdEdx = (TH3D *)embFile->Get("MatchedTrackQAPlots/matchTrackRapidityPtNHitsdEdxHisto");
  TH3D *primaryNHitsdEdx = (TH3D *)dataFile->Get(Form("%s/%s_%s/%s_ypTnHitsdEdxPostCuts",
						      name.Data(),name.Data(),particleType.Data(),
						      name.Data()));

  TGraphErrors *matchNHitsdEdxGraph[npTBins];
  TGraphErrors *primaryNHitsdEdxGraph[npTBins];

  for (int pTIndex=0; pTIndex<npTBins; pTIndex++){

    matchNHitsdEdxGraph[pTIndex] = new TGraphErrors();
    matchNHitsdEdxGraph[pTIndex]->SetTitle(Form("<nHitsdEdx> Vs Rapidity, pT = [%g,%g] GeV;y;<nHitsdEdx>",pTIndex*.4,(pTIndex+1)));
    primaryNHitsdEdxGraph[pTIndex] = new TGraphErrors();

    for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){

      TH1D *hMatch = GetZProjectionHisto1D(matchNHitsdEdx,
					   yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					   pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu nHitsdEdx");
      TH1D *hPrimary = GetZProjectionHisto1D(primaryNHitsdEdx,
					     yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					     pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu nHitsdEdx");

      matchNHitsdEdxGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hMatch->GetMean());
      matchNHitsdEdxGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hMatch->GetRMS()/sqrt(hMatch->GetEntries()));
      
      primaryNHitsdEdxGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hPrimary->GetMean());
      primaryNHitsdEdxGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hPrimary->GetRMS()/sqrt(hPrimary->GetEntries()));
      
      hMatch->Delete();
      hPrimary->Delete();    
      
    }

    object[pTIndex].push_back(matchNHitsdEdxGraph[pTIndex]);
    object[pTIndex].push_back(primaryNHitsdEdxGraph[pTIndex]);
    cout <<"push" <<endl;
    
    ((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMinimum(0);
    ((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMaximum(50);
    ((TGraphErrors *)object[pTIndex][0])->SetMarkerColor(kRed);
    ((TGraphErrors *)object[pTIndex][0])->SetLineColor(kRed);
    ((TGraphErrors *)object[pTIndex][1])->SetMarkerColor(kBlue);
    ((TGraphErrors *)object[pTIndex][1])->SetLineColor(kBlue);
    
    objectOpts[0][0] = "AP";
    objectOpts[0][1] = "PZ";

  }//End Loop Over pTindex
  GeneratePage("Average nHitsdEdx",globalLegend);
  
  //*****************************************************************
  //    ndEdxHits   (in y and pT bins)
  //*****************************************************************
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){
    for (int pTIndex=0; pTIndex<npTBins; pTIndex++){
      
      object[pTIndex].push_back(GetZProjectionHisto1D(matchNHitsdEdx,
      						      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
      						      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"nHitsdEdx"));
      object[pTIndex].push_back(GetZProjectionHisto1D(primaryNHitsdEdx,
						      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
						      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"nHitsdEdx"));

      Normalize((TH1D*)object[pTIndex][0]);
      Normalize((TH1D*)object[pTIndex][1]);
      
      ((TH1D *)object[pTIndex][0])->GetYaxis()->SetRangeUser(0,TMath::Max(
									  ((TH1D *)object[pTIndex][0])->GetMaximum(),
									  ((TH1D *)object[pTIndex][1])->GetMaximum())*1.25);
      
      ((TH1D *)object[pTIndex][0])->SetMarkerColor(kRed);
      ((TH1D *)object[pTIndex][0])->SetLineColor(kRed);
      ((TH1D *)object[pTIndex][1])->SetMarkerColor(kBlue);
      ((TH1D *)object[pTIndex][1])->SetLineColor(kBlue);
      
      objectOpts[pTIndex][0] = "";
      objectOpts[pTIndex][1] = "SAME";
            
    }//End Loop Over pTIndex

    GeneratePage(Form("nHitsdEdx Distributions y=%.02g",yRangeMin+yIndex*yBinWidth),globalLegend);
    
  }//End Loop Over yIndex
  
  //*****************************************************************
  //    <globalDCA>  vs y (pT bins)
  //*****************************************************************
  TH3D *matchGlobalDCA = (TH3D *)embFile->Get("MatchedTrackQAPlots/matchTrackRapidityPtGlobalDCAHisto");
  TH3D *primaryGlobalDCA = (TH3D *)dataFile->Get(Form("%s/%s_%s/%s_yglobalDCAPostCuts",
						  name.Data(),name.Data(),particleType.Data(),
						  name.Data()));
  cout <<"GlobalDCA" <<primaryGlobalDCA <<endl;
  
  TGraphErrors *matchGlobalDCAGraph[npTBins];
  TGraphErrors *primaryGlobalDCAGraph[npTBins];

  for (int pTIndex=0; pTIndex<npTBins; pTIndex++){

    matchGlobalDCAGraph[pTIndex] = new TGraphErrors();
    matchGlobalDCAGraph[pTIndex]->SetTitle(Form("<GlobalDCA> Vs Rapidity, pT = [%g,%g] GeV;y;<GlobalDCA>",(Double_t)pTIndex*.4,(Double_t)(pTIndex+1)));
    primaryGlobalDCAGraph[pTIndex] = new TGraphErrors();

    for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){

      TH1D *hMatch = GetZProjectionHisto1D(matchGlobalDCA,
					   yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					   pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu GlobalDCA");
      TH1D *hPrimary = GetZProjectionHisto1D(primaryGlobalDCA,
					     yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
					     pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"#mu GlobalDCA");

      cout <<hPrimary <<endl;
      
      matchGlobalDCAGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hMatch->GetMean());
      matchGlobalDCAGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hMatch->GetRMS()/sqrt(hMatch->GetEntries()));
      
      primaryGlobalDCAGraph[pTIndex]->SetPoint(yIndex,yRangeMin+yIndex*yBinWidth,hPrimary->GetMean());
      primaryGlobalDCAGraph[pTIndex]->SetPointError(yIndex,yBinWidth/2.0,hPrimary->GetRMS()/sqrt(hPrimary->GetEntries()));
      
      hMatch->Delete();
      hPrimary->Delete();
      
    }//End Loop Over yIndex

    object[pTIndex].push_back(matchGlobalDCAGraph[pTIndex]);
    object[pTIndex].push_back(primaryGlobalDCAGraph[pTIndex]);
    cout <<"push" <<endl;
    
    ((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMinimum(0);
    ((TGraphErrors *)object[pTIndex][0])->GetHistogram()->SetMaximum(.5);
    ((TGraphErrors *)object[pTIndex][0])->SetMarkerColor(kRed);
    ((TGraphErrors *)object[pTIndex][0])->SetLineColor(kRed);
    ((TGraphErrors *)object[pTIndex][1])->SetMarkerColor(kBlue);
    ((TGraphErrors *)object[pTIndex][1])->SetLineColor(kBlue);
    
    objectOpts[pTIndex][0] = "AP";
    objectOpts[pTIndex][1] = "P";
    
  }//End Loop Over pTIndex
  GeneratePage("Average GlobalDCA",globalLegend);

  //*****************************************************************
  //    globalDCA  Distributions in y and pT bins
  //*****************************************************************
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){
    
    for (int pTIndex=0; pTIndex<npTBins; pTIndex++){
            
      object[pTIndex].push_back(GetZProjectionHisto1D(matchGlobalDCA,
      						      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
      						      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"GlobalDCA"));
      object[pTIndex].push_back(GetZProjectionHisto1D(primaryGlobalDCA,
							      yRangeMin+yIndex*yBinWidth, yRangeMin+yIndex*yBinWidth,
							      pTIndex*.4,(pTIndex+1)*.4,TString::Format("%d",pTIndex),"GlobalDCA"));

      Normalize((TH1D*)object[pTIndex][0]);
      Normalize((TH1D*)object[pTIndex][1]);
      
      ((TH1D *)object[pTIndex][0])->GetXaxis()->SetRangeUser(0,3);
      ((TH1D *)object[pTIndex][0])->GetYaxis()->SetRangeUser(0,TMath::Max(
									  ((TH1D *)object[pTIndex][0])->GetMaximum(),
									  ((TH1D *)object[pTIndex][1])->GetMaximum())*1.25);
      
      ((TH1D *)object[pTIndex][0])->SetMarkerColor(kRed);
      ((TH1D *)object[pTIndex][0])->SetLineColor(kRed);
      ((TH1D *)object[pTIndex][1])->SetMarkerColor(kBlue);
      ((TH1D *)object[pTIndex][1])->SetLineColor(kBlue);
      
      objectOpts[pTIndex][0] = "";
      objectOpts[pTIndex][1] = "SAME";
      

      
    }//End Loop Over pTIndex

    GeneratePage(Form("Global DCA Distributions y=%.02g",yRangeMin+yIndex*yBinWidth),globalLegend);
    
  }//End Loop Over yIndex
  
  /*
  //*****************************************************************
  //    dEdx Recentered of Primary Tracks
  //*****************************************************************
  TH3D *tpcRecenter = (TH3D *)dataFile->Get(Form("%s/%s_%s/%s_RecenteredZTPC",
						 name.Data(),name.Data(),particleType.Data(),
						 name.Data()));
  objIndex=0;
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){

    object[objIndex].push_back(GetZProjectionHisto2D(tpcRecenter,
						       yRangeMin+yIndex*yBinWidth,yRangeMin+yIndex*yBinWidth,
						       0,3,TString::Format("%d",yIndex),""));
    
    objectOpts[objIndex][0] = "COL";

    padOpts[0] = "LOGZ";
    padOpts[1] = "LOGZ";
    padOpts[2] = "LOGZ";
    padOpts[3] = "LOGZ";
    padOpts[4] = "LOGZ";
    padOpts[5] = "LOGZ";
    
    if ((objIndex>1 && objIndex%5 == 0) || yIndex == nRapidityBins-1){
      
      GeneratePage("Recentered dEdx: Z_{TPC}");
      objIndex = 0;
    }
    else
      objIndex++;    
  }
  
  
  //*****************************************************************
  //    InverseBeta Recentered of Primary Tracks
  //*****************************************************************
  TH3D *tofRecenter = (TH3D *)dataFile->Get(Form("%s/%s_%s/%s_RecenteredZTOF",
						 name.Data(),name.Data(),particleType.Data(),
						 name.Data()));
  objIndex=0;
  for (int yIndex=GetMinRapidityIndexOfInterest(); yIndex<GetMaxRapidityIndexOfInterest(); yIndex++){

    object[objIndex].push_back(GetZProjectionHisto2D(tofRecenter,
						       yRangeMin+yIndex*yBinWidth,yRangeMin+yIndex*yBinWidth,
						       0,3,TString::Format("%d",yIndex),""));
    
    objectOpts[objIndex][0] = "COL";

    padOpts[0] = "LOGZ";
    padOpts[1] = "LOGZ";
    padOpts[2] = "LOGZ";
    padOpts[3] = "LOGZ";
    padOpts[4] = "LOGZ";
    padOpts[5] = "LOGZ";
    
    if ((objIndex>1 && objIndex%5 == 0) || yIndex == nRapidityBins-1){
      
      GeneratePage("Recentered dEdx: Z_{TOF}");
      objIndex = 0;
    }
    else
      objIndex++;    
  }
  */
  
  //gSystem->Sleep(3000);
  delete pageCanvas;

  //Combine all the lose pages into one document
  if (save){
    //gSystem->Exec(Form("pdfunite Page*.pdf %s && rm Page*.pdf",outDocument.Data()));
    gSystem->Exec(Form("gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=%s %s/*Page*.pdf",outDocument.Data(),
		       outDir.Data()));
    gSystem->Exec(Form("rm %s/*Page*.pdf",outDir.Data()));
  }
  
}

//__________________________________________________________________________________
void GeneratePage(TString pageTitle, TLegend *leg){
  cout <<Form("Generating Page %s",pageTitle.Data()) <<endl;
  nPage++;
  titleBar->Clear();
  titleBar->AddText(pageTitle);
  titleBar->Draw();

  pageNum->Clear();
  pageNum->AddText(Form("%d",nPage));
  pageNum->Draw();
  
  Double_t spacing = .02;
  Double_t padWidth = .9/2.0 - spacing;
  Double_t padHeight = .9/3.0 - 2.*spacing;


  pad[0] = new TPad("pad0","",
		    leftMargin,
		    topMargin-padHeight,
		    leftMargin+padWidth,
		    topMargin,kWhite);

  pad[1] = new TPad("pad1","",
		    leftMargin+padWidth+2*spacing,
		    topMargin-padHeight,
		    leftMargin+padWidth+padWidth+2*spacing,
		    topMargin,kWhite);

  pad[2] = new TPad("pad2","",
		    leftMargin,
		    topMargin-padHeight-2*spacing,
		    leftMargin+padWidth,
		    topMargin-padHeight-2*spacing-padHeight,kWhite);

  pad[3] = new TPad("pad3","",
		    leftMargin+padWidth+2*spacing,
		    topMargin-padHeight-2*spacing,
		    leftMargin+padWidth+padWidth+2*spacing,
		    topMargin-padHeight-2*spacing-padHeight,kWhite);
 
  pad[4] = new TPad("pad4","",
		    leftMargin,
		    topMargin-2*padHeight-4*spacing,
		    leftMargin+padWidth,
		    topMargin-2*padHeight-4*spacing-padHeight,kWhite);

  pad[5] = new TPad("pad5","",
		    leftMargin+padWidth+2*spacing,
		    topMargin-2*padHeight-4*spacing,
		    leftMargin+padWidth+padWidth+2*spacing,
		    topMargin-2*padHeight-4*spacing-padHeight,kWhite);

  for (int i=0; i<6; i++){
    pad[i]->Draw();
    pad[i]->cd();
    
    if (padOpts[i] == "LOGY")
      pad[i]->SetLogy();
    else if (padOpts[i] == "LOGZ")
      pad[i]->SetLogz();
    
    cout <<"pad: " <<i <<endl;
    for (unsigned int j=0; j<object[i].size(); j++){
      cout <<object[i][j];// <<endl;
      object[i][j]->Draw(objectOpts[i][j]);
      cout <<" drawn" <<endl;
    }//End Loop Over Objects in pad

    pageCanvas->cd();
  }//End Loop Over Pads

  if (leg != NULL){
    pad[5]->cd();
    cout <<"Drawing Legend" <<endl;
    leg->Draw("SAME");    
  }


  pageCanvas->Update();
  //return;
  if (save){
    pageCanvas->Print(Form("%s/%s_Page_%03d.pdf",outDir.Data(),name.Data(),nPage),"pdf");
    //gSystem->Exec(Form("epstopdf Page_%03d.eps && rm Page_%03d.eps",nPage,nPage));
    //gSystem->Exec(Form("convert -compress lossless Page_%03d.eps Page_%03d.pdf && rm Page_%03d.eps",nPage,nPage,nPage));
    
  }
  
  cout <<"here" <<endl;
  
  //gSystem->Sleep(500);
  pageCanvas->Clear();
  //return;
  for (int i=0; i<6; i++){
    for (unsigned int j=0; j<object[i].size(); j++){
      if (object.at(i).at(j)){
	object.at(i).at(j)->Delete();
	//object[i][j] = NULL;
      }
    }
    object[i].resize(0,(TObject *)NULL);
    //objectOpts[i].resize(1,(char *)NULL);
    objectOpts[i].resize(1,"");
    padOpts[i] = "";
   
   
  }

  cout <<"here2" <<endl;
  
}

//_______________________________________________________________________________
TH1D *GetZProjectionHisto1D (TH3 *h3, Double_t xMin, Double_t xMax,
			     Double_t yMin, Double_t yMax,TString nameSuffix, TString titleBase){

  Int_t xLowBin = h3->GetXaxis()->FindBin(xMin);
  Int_t xHighBin = h3->GetXaxis()->FindBin(xMax);
  Int_t yLowBin = h3->GetYaxis()->FindBin(yMin);
  Int_t yHighBin = h3->GetYaxis()->FindBin(yMax);
  
  TH1D *zHisto = h3->ProjectionZ(Form("%s_pz_%s", h3->GetName(),nameSuffix.Data()),
					xLowBin,xHighBin,yLowBin,yHighBin);

  zHisto->GetYaxis()->SetRangeUser(0,zHisto->GetMaximum()*1.25);

  zHisto->SetTitle(Form("%s: %s = [%.2g,%.2g], %s = [%.2g,%.2g]",titleBase.Data(),
			h3->GetXaxis()->GetTitle(),xMin,xMax,
			h3->GetYaxis()->GetTitle(),yMin,yMax));

  zHisto->GetYaxis()->SetTitle(Form("(1/N_{Track})dN/%s",zHisto->GetXaxis()->GetTitle()));
  zHisto->GetYaxis()->SetTitleOffset(1.3);
  
  return zHisto;

}

//_______________________________________________________________________________
TH2D *GetZProjectionHisto2D (TH3 *h3, Double_t xMin, Double_t xMax,
			     Double_t yMin, Double_t yMax,TString nameSuffix, TString titleBase){

  Int_t xLowBin = h3->GetXaxis()->FindBin(xMin);
  Int_t xHighBin = h3->GetXaxis()->FindBin(xMax);
  Int_t yLowBin = h3->GetYaxis()->FindBin(yMin);
  Int_t yHighBin = h3->GetYaxis()->FindBin(yMax);

  cout <<xLowBin <<" " <<xHighBin <<" " <<yLowBin <<" " <<yHighBin <<endl;
  
  h3->GetXaxis()->SetRange(xLowBin,xHighBin);
  h3->GetYaxis()->SetRange(yLowBin,yHighBin);
  //h3->GetXaxis()->SetRangeUser(xMin-yBinWidth/2.0,xMax+yBinWidth/2.0); 
  
  TH1D *htemp =  (TH1D *)h3->Project3D(Form("zy")); 
  TH2D *h2 = (TH2D*)gDirectory->Get(Form("%s_zy",h3->GetName()));
  h2->SetName(Form("%s_%s",h2->GetName(),nameSuffix.Data()));

  //gDirectory->Print();
  //cout <<Form("%s_zy",h3->GetName(),nameSuffix.Data()) <<endl;
  
  h2->SetTitle(Form("%s: %s = [%.2g,%.2g], %s = [%.2g,%.2g];%s;%s",titleBase.Data(),
		    h3->GetXaxis()->GetTitle(),xMin,xMax,
		    h3->GetYaxis()->GetTitle(),yMin,yMax,
		    h3->GetYaxis()->GetTitle(),
		    h3->GetZaxis()->GetTitle()));
  
  //htemp->Delete();
  
  return h2;
}
  
//_______________________________________________________________________________
TH1D *GetXProjectionHisto1D (TH3 *h3, Double_t yMin, Double_t yMax,
			     Double_t zMin, Double_t zMax,TString nameSuffix, TString titleBase){

  Int_t yLowBin = h3->GetYaxis()->FindBin(yMin);
  Int_t yHighBin = h3->GetYaxis()->FindBin(yMax);
  Int_t zLowBin = h3->GetZaxis()->FindBin(zMin);
  Int_t zHighBin = h3->GetZaxis()->FindBin(zMax);

  TH1D *zHisto = h3->ProjectionX(Form("%s_px_%s", h3->GetName(),nameSuffix.Data()),
					yLowBin,yHighBin,zLowBin,zHighBin);

  zHisto->GetYaxis()->SetRangeUser(0,zHisto->GetMaximum()*1.25);

  zHisto->SetTitle(Form("%s: %s = [%.2g,%.2g], %s = [%.2g,%.2g]",titleBase.Data(),
			h3->GetYaxis()->GetTitle(),yMin,yMax,
			h3->GetZaxis()->GetTitle(),zMin,zMax));

  zHisto->GetYaxis()->SetTitle(Form("(1/N_{Track})dN/%s",zHisto->GetXaxis()->GetTitle()));
  zHisto->GetYaxis()->SetTitleOffset(1.3);

  return zHisto;

}

//________________________________________________________________________________
void Normalize(TH1D *h){
  
  //Make Sure the histogram has entries
  if (h->GetEntries() == 0)
    return;

  h->Scale(1.0/(h->GetEntries() * h->GetBinWidth(1)));

}
