//This code uses the output of the backgroudn simulations to determine
//the fractional background due to feed down for the pions, kaons, and protons

#include <iostream>
#include <vector>
#include <utility>

#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TH2.h>
#include <TH3.h>
#include <TMath.h>
#include <TF1.h>
#include <TSystem.h>
#include <TPaveText.h>
#include <TLegend.h>
#include <TGraphAsymmErrors.h>
#include <TVirtualFitter.h>
#include <TDirectory.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "ParticleInfo.h"
#include "UserCuts.h"
#include "StyleSettings.h"

Bool_t draw=false;
Bool_t save=true;


//____MAIN__________________________________________
void feeddownBackground(TString inputFile, TString outputFile){

  //Create an instance of the ParticleInfo Class
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion());

  //Get the Number of Centrality Bins
  const unsigned int nCentralityBins = GetNCentralityBins();

  //Get the Collision Energy
  Double_t energy = GetCollisionEnergy();

  //Define the Particle Species to be used
  const int nSpecies(3);
  const int nCharges(2);
  std::vector<int> charge(nCharges);
  charge.at(0) = -1;
  charge.at(1) = 1;
  
  //Open the inputFile and Load the Histograms
  TFile *inFile = new TFile(inputFile,"READ");
  inFile->cd("FeedDown");
  std::vector< std::vector < std::vector<TH3F *> > > parentIDHisto3D
    (nSpecies, std::vector< std::vector<TH3F *> >
     (nCharges, std::vector<TH3F *>
      (nCentralityBins, (TH3F *)NULL)));
  
  for (int iSpecies=0; iSpecies<nSpecies; iSpecies++){
    for (int iCharge=0; iCharge<nCharges; iCharge++){
      for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
	
	parentIDHisto3D.at(iSpecies).at(iCharge).at(iCentBin) =
	  (TH3F *)gDirectory->Get(Form("parentGeantIDHisto_%s_cent%02d",
				       particleInfo->GetParticleName(iSpecies,charge.at(iCharge)).Data(),
				       iCentBin));
	
      }//End Loop Over Centrality Bins
    }//End Loop Over Charges
  }//End Loop Over Species

  //Open the output file
  TFile *outFile = new TFile(outputFile,save ? "UPDATE":"READ");
  outFile->cd();
  
  //Drawing Canvas
  TCanvas *canvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    //    canvas->SetLogy();
    canvas->SetTicks();
  }

  //Define the Graphs that will hold the background fraction and their fits
  std::vector< std::vector < std::vector< std::vector< TGraphAsymmErrors *> > > > feeddownGraph
    (nSpecies, std::vector< std::vector< std::vector< TGraphAsymmErrors *> > >
     (nCharges, std::vector< std::vector< TGraphAsymmErrors *> >
      (nCentralityBins, std::vector< TGraphAsymmErrors*>
       (nRapidityBins, (TGraphAsymmErrors *)NULL))));
  std::vector< std::vector < std::vector< std::vector< TF1 *> > > > feeddownGraphFit
    (nSpecies, std::vector< std::vector< std::vector< TF1 *> > >
     (nCharges, std::vector< std::vector< TF1 *> >
      (nCentralityBins, std::vector< TF1*>
       (nRapidityBins, (TF1 *)NULL))));
  std::vector< std::vector < std::vector< std::vector< TF1 *> > > > feeddownGraphFit1
    (nSpecies, std::vector< std::vector< std::vector< TF1 *> > >
     (nCharges, std::vector< std::vector< TF1 *> >
      (nCentralityBins, std::vector< TF1*>
       (nRapidityBins, (TF1 *)NULL))));
  std::vector< std::vector < std::vector< std::vector< TGraphErrors *> > > > feeddownGraphFitSys
    (nSpecies, std::vector< std::vector< std::vector< TGraphErrors *> > >
     (nCharges, std::vector< std::vector< TGraphErrors *> >
      (nCentralityBins, std::vector< TGraphErrors*>
       (nRapidityBins, (TGraphErrors *)NULL))));
  std::vector< std::vector < std::vector< std::vector< TGraphErrors *> > > > feeddownGraphFitSys1
    (nSpecies, std::vector< std::vector< std::vector< TGraphErrors *> > >
     (nCharges, std::vector< std::vector< TGraphErrors *> >
      (nCentralityBins, std::vector< TGraphErrors*>
       (nRapidityBins, (TGraphErrors *)NULL))));

  for (int iSpecies=0; iSpecies<nSpecies; iSpecies++){
    for (int iCharge=0; iCharge<nCharges; iCharge++){

      //If this is for protons then the yAxis means the combined feed-down and knockout background
      TString yAxisTitleMod = " ";
      if (iSpecies == PROTON && iCharge > 0)
	yAxisTitleMod = " + Knockout ";
      
      for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
	for (int yIndex=0; yIndex<nRapidityBins; yIndex++){

	  //Graphs
	  feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex) = new TGraphAsymmErrors();
	  feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->
	    SetName(Form("feeddownBackground_%s_cent%02d_yIndex%02d",
			 particleInfo->GetParticleName(iSpecies,charge.at(iCharge)).Data(),
			 iCentBin,yIndex));
	  feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->
	    SetTitle(Form("%s Feed Down%sBackground | Cent=[%02d,%02d]%% | y_{%s}=[%.02f,%.02f];(m_{T}-m_{%s})^{Reco} (GeV/c^{2});Feed Down%sFraction",
			  particleInfo->GetParticleSymbol(iSpecies,charge.at(iCharge)).Data(),
			  yAxisTitleMod.Data(),
			  iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
			  (int)GetCentralityPercents().at(iCentBin),
			  particleInfo->GetParticleSymbol(iSpecies,charge.at(iCharge)).Data(),
			  GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
			  particleInfo->GetParticleSymbol(iSpecies,charge.at(iCharge)).Data(),
			  yAxisTitleMod.Data()));

	  feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->
	    SetMarkerStyle(particleInfo->GetParticleMarker(iSpecies,charge.at(iCharge)));
	  feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->
	    SetMarkerColor(kBlack);

	  //Fits
	  if (iSpecies == PION)
	    feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex) =
	      new TF1(Form("%s_FitExp",feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetName()),
		      "[0]*TMath::Exp([1]*x)+[2]",0.04,2);
	  else
	    feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex) =
	      new TF1(Form("%s_FitExp",feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetName()),
		      "[0]*TMath::Exp([1]*x)",0.04,2);
	  
	  feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex) =
	    new TF1(Form("%s_FitPower",feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetName()),
		    "[0]*TMath::Power(x,[1])",0.04,2);
	  

	  feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetParameters(0.08,-2.0);
	  feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetParLimits(0,0,0.7);
	  feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetParLimits(1,-10,0);
	  if (iSpecies == PION){
	    feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetParameter(2,0.02);
	    feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetParLimits(2,0,.05);
	  }
	  
	  feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetLineWidth(3);
	  feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetLineColor(kRed);
	  feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetLineStyle(1);

	  feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetParameters(1.0,-1.0);
	  feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetParLimits(0,0,5.0);
	  feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetParLimits(1,-2,0);
	  
	  
	  feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetLineWidth(3);
	  feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetLineColor(kBlue);
	  feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetLineStyle(9);

	}//End Loop Over yIndex
      }//End Loop Over Centrality Bins
    }//End Loop Over Charges
  }//End Loop Over Species
  
  //Loop Over the species and Compute the Feed Down Background Fraction
  TH1D *parentIDHisto = NULL;
  TH3F *localParentIDHisto3D = NULL;
  for (int iSpecies=0; iSpecies<nSpecies; iSpecies++){
    for (int iCharge=0; iCharge<nCharges; iCharge++){

      outFile->cd();
      outFile->mkdir(Form("%s",particleInfo->GetParticleName(iSpecies,charge.at(iCharge)).Data()));
      outFile->cd(Form("%s",particleInfo->GetParticleName(iSpecies,charge.at(iCharge)).Data()));

      gDirectory->mkdir("FeedDownContributionHistos");
      gDirectory->cd("FeedDownContributionHistos");
      
      for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

	localParentIDHisto3D = parentIDHisto3D.at(iSpecies).at(iCharge).at(iCentBin);
	
	//Loop Over the rapidity and mTm0 Bins
	for (int xBin=1; xBin<=localParentIDHisto3D->GetNbinsX(); xBin++){

	  //Convert the histogram bin indices to the rapidity indidces
	  int yIndex = GetRapidityIndex(localParentIDHisto3D->GetXaxis()->GetBinCenter(xBin));

	  TH1D totalHisto("totalHisto","totalHisto",localParentIDHisto3D->GetNbinsY(),mTm0Min,mTm0Max);
	  TH1D passHisto("passHisto","passHisto",localParentIDHisto3D->GetNbinsY(),mTm0Min,mTm0Max);
	  
	  for (int yBin=1; yBin<localParentIDHisto3D->GetNbinsY(); yBin++){

	    int mTm0Index = GetmTm0Index(localParentIDHisto3D->GetYaxis()->GetBinCenter(yBin));
	    parentIDHisto = localParentIDHisto3D->ProjectionZ("pz",xBin,xBin,yBin,yBin);

	    parentIDHisto->SetName(Form("feeddownContribution_%s_cent%02d_yIndex%02d_mTm0Index%02d",
					particleInfo->GetParticleName(iSpecies,charge.at(iCharge)).Data(),
					iCentBin,yIndex,mTm0Index));
	    parentIDHisto->SetTitle(Form("Parent GeantID Distribution of %s | #sqrt{s_{NN}}=%.01f GeV | Cent=[%02d,%02d]%% | y_{%s}=[%.02f,%.02f] | m_{T}-m_{%s}=[%.03f,%.03f] (GeV/c^{2})",
					 particleInfo->GetParticleSymbol(iSpecies,charge.at(iCharge)).Data(),
					 energy,
					 iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
					 (int)GetCentralityPercents().at(iCentBin),
					 particleInfo->GetParticleSymbol(iSpecies,charge.at(iCharge)).Data(),
					 localParentIDHisto3D->GetXaxis()->GetBinLowEdge(xBin),
					 localParentIDHisto3D->GetXaxis()->GetBinLowEdge(xBin) + localParentIDHisto3D->GetXaxis()->GetBinWidth(1),
					 particleInfo->GetParticleSymbol(iSpecies,charge.at(iCharge)).Data(),
					 localParentIDHisto3D->GetYaxis()->GetBinLowEdge(yBin),
					 localParentIDHisto3D->GetYaxis()->GetBinLowEdge(yBin) + localParentIDHisto3D->GetYaxis()->GetBinWidth(1)));

	    //Make sure there are enough entries
	    if (parentIDHisto->GetEntries() <= 5){
	      delete parentIDHisto;
	      continue;
	    }

	    //Set Bin Labels
	    for (int i=1; i<parentIDHisto->GetNbinsX(); i++){
	      parentIDHisto->GetXaxis()->SetBinLabel(i,GetParticleSymbolUsingGeantID(i-1));
	    }
	    parentIDHisto->GetXaxis()->LabelsOption("d");
	    parentIDHisto->GetXaxis()->SetRangeUser(0,32);
	    parentIDHisto->SetFillColor(GetCentralityColor(iCentBin));
	    parentIDHisto->GetYaxis()->SetTitle("Counts");
	    
	    Double_t total = parentIDHisto->Integral(parentIDHisto->FindBin(0),parentIDHisto->FindBin(32));
	    Double_t pass = parentIDHisto->Integral(parentIDHisto->FindBin(1),parentIDHisto->FindBin(32));

	    totalHisto.SetBinContent(yBin,total);
	    passHisto.SetBinContent(yBin,pass);

	    parentIDHisto->Write(parentIDHisto->GetName(),TObject::kOverwrite);	    
	    
	    delete parentIDHisto;
	    parentIDHisto = NULL;
	    
	  }//End Loop Over mTm0 axis
	  
	  passHisto.Rebin(2);
	  totalHisto.Rebin(2);
	  feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->BayesDivide(&passHisto,&totalHisto);

	  //Skip Graphs with too few points
	  if (feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetN() < 2){
	    delete feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex);
	    delete feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex);
	    delete feeddownGraphFitSys.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex);
	    
	    feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex) = NULL;
	    feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex) = NULL;
	    feeddownGraphFitSys.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex) = NULL;
	    
	    continue;
	  }

	  //Clean up the Graphs
	  TGraphChop((TGraphErrors *)feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex),.05,true);
	  TGraphChop((TGraphErrors *)feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex),1.5,false);

	  //Get the min X value of the first point in the graph
	  cout <<feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex) <<endl;
	  cout <<feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetN() <<endl;
	  Double_t minX = feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetX()[0] -
	    feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetEXlow()[0];

	  //Fit the Graph and Get the Confidence Interval
	  feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetRange(minX,2);
	  feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->
	    Fit(feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex),"EX0R");
	  feeddownGraphFitSys.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex) =
	    GetConfidenceIntervalOfFit(feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex));

	  feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->SetRange(minX,2);
	  feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->
	    Fit(feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex),"EX0R+");
	  feeddownGraphFitSys1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex) =
	    GetConfidenceIntervalOfFit(feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex));
	  
	  if (draw){
	    canvas->SetLogy(0);
	    feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->Draw("APZ");
	    feeddownGraphFitSys.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->Draw("3");
	    canvas->Update();
	    gSystem->Sleep(10);
	  }

	  
	}//End Loop Over rapidity axis
	
	
      }//End Loop Over centrality bins
    }//End Loop Over charges
  }//End Loop Over species

  //Save Everything if Requested
  if (!save)
    return;

  for (int iSpecies=0; iSpecies<nSpecies; iSpecies++){
    for (int iCharge=0; iCharge<nCharges; iCharge++){

      outFile->cd();
      if (!outFile->cd(Form("%s",particleInfo->GetParticleName(iSpecies,charge.at(iCharge)).Data()))){
	outFile->mkdir(Form("%s",particleInfo->GetParticleName(iSpecies,charge.at(iCharge)).Data()));
	outFile->cd(Form("%s",particleInfo->GetParticleName(iSpecies,charge.at(iCharge)).Data()));
      }

      gDirectory->mkdir("FeedDownBackgroundGraphs");
      gDirectory->mkdir("FeedDownBackgroundFits");
      
      for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
	for (int yIndex=0; yIndex<nRapidityBins; yIndex++){
	  
	  gDirectory->cd("FeedDownBackgroundGraphs");
	  if (!feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)){
	    gDirectory->cd("..");
	    continue;
	  }
	  feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->
	    Write(feeddownGraph.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetName(),
		  TObject::kOverwrite);
	  
	  gDirectory->cd("..");
	  gDirectory->cd("FeedDownBackgroundFits");
	  feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->
	    Write(feeddownGraphFit.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetName(),
		  TObject::kOverwrite);
	  feeddownGraphFitSys.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->
	    Write(feeddownGraphFitSys.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetName(),
		  TObject::kOverwrite);
	  feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->
	    Write(feeddownGraphFit1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetName(),
		  TObject::kOverwrite);
	  feeddownGraphFitSys1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->
	    Write(feeddownGraphFitSys1.at(iSpecies).at(iCharge).at(iCentBin).at(yIndex)->GetName(),
		  TObject::kOverwrite);

	  gDirectory->cd("..");
	  
	}//End Loop Over yIndex
      }//End Loop Over Cent Bin
    }//End Loop Over Charge
  }//End Loop Over Species  
      
}
