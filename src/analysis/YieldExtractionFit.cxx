#include <iostream>
#include <utility>
#include <map>
#include <assert.h>
#include <algorithm>

#include <TF1.h>
#include <TH1.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TList.h>
#include <TSystem.h>
#include <TLine.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "ParticleInfo.h"
#include "YieldExtractionFit.h"

//_______________________________________________________
YieldExtractionFit::YieldExtractionFit(){

  isTPCMode = true;
  
  particleInfo = NULL;
  minEntries = 100;
  
  pid = 0;
  yIndex = 0;
  mTm0Index = 0;

  rapidity = mTm0 = pT = mT = pTotal = mass = pZ = 0;

  yield = yieldErr = 0.0;

  dataHisto = NULL;
  fitFunc = NULL;
  confInterval = NULL;
  yieldGraph = NULL;
  notYieldGraph = NULL;
  
  tofOptFits;

  electronAmp = -1;
  electronMax = -1;
  slushParam = -1;
  
  minFitRange = 99999;
  maxFitRange = -99999;
  minFitLine = NULL;
  maxFitLine = NULL;
}

//_______________________________________________________
YieldExtractionFit::YieldExtractionFit(int particleSpecies, int yBin, int mTm0Bin,
				       TH1D *dHisto, ParticleInfo *pInfo,
				       std::vector<TF1 *> tofOptVec){

  //Since this constructor takes the tof Optimized Histograms
  //then we must be in TPC mode
  isTPCMode = true;
  
  //Bin Info
  pid = particleSpecies;
  yIndex = yBin;
  mTm0Index = mTm0Bin;
  minEntries = 100;

  //Kinematic Info
  mass = particleInfo->GetParticleMass(pid);
  rapidity = GetRapidityRangeCenter(yIndex);
  mTm0 = GetmTm0RangeCenter(mTm0Index);
  mT = mTm0 + mass;
  pT = ConvertmTm0ToPt(mTm0,mass);
  pZ = ComputepZ(mT,rapidity);
  pTotal = ComputepTotal(pT,pZ);

  yield = 0.0;
  yieldErr = 0.0;

  electronAmp = -1;
  electronMax = -1;
  slushParam = -1;

  particleInfo = pInfo;
  dataHisto = dHisto;
  minFitRange = 99999;
  maxFitRange = -99999;

  minFitLine = NULL;
  maxFitLine = NULL;

  fitFunc = NULL;
  confInterval = NULL;
  yieldGraph = NULL;
  notYieldGraph = NULL;

  tofOptFits = tofOptVec;

  //Set the Predicted Means
  for (unsigned int i=0; i<tofOptFits.size(); i++){
    predictedMean.push_back(particleInfo->PredictZTPC(pTotal,pid,i));
  }

  for (unsigned int i=0; i<predictedMean.size(); i++){    
    minFitRange = TMath::Min(minFitRange,predictedMean.at(i));
    maxFitRange = TMath::Max(maxFitRange,predictedMean.at(i));
  }
  minFitRange -= .5;
  maxFitRange += .5;

  //Create the Single Distributions
  for (unsigned int i=0; i<tofOptFits.size(); i++){
    
    if (!tofOptFits.at(i)){
      maxFitRange = TMath::Min(maxFitRange,predictedMean.at(i) - 5*.08);
      continue;
    }

    singleDistributions.push_back(new TF1(Form("%s_Fit_%s",
					       dataHisto->GetName(),
					       particleInfo->GetParticleName(i).Data()),
					  this,&YieldExtractionFit::singleDistFunc,
					  minFitRange,maxFitRange,3,"YieldExtractionFit","singleDistFunc"));
    singleDistributions.back()->FixParameter(0,i); //Index of TofOptFits
    singleDistributions.back()->SetLineColor(particleInfo->GetParticleColor(i));
    singleDistributions.back()->SetNpx(1000);
    fitParNames.push_back(TString::Format("%s",particleInfo->GetParticleSymbol(i).Data()));
    
    pidToFitParMap[i] = singleDistributions.size()-1;
    finalFitAmpPars.push_back(std::make_pair(0,false));

  }

  //Create the Full Fit
  //The number of parameters in the fit is the number of amplitudes (equal to the size of singleDistributions)
  //plus the slush parameter
  fitFunc = new TF1(Form("%s_Fit",dataHisto->GetName()),this,&YieldExtractionFit::yieldExtractFitTPC,
		    minFitRange,maxFitRange,singleDistributions.size()+1,"YieldExtractionFit","yieldExtractFitTPC");
  fitFunc->SetNpx(10000);
  fitParNames.push_back("Slush");

  for (int iPar=0; iPar<fitFunc->GetNpar(); iPar++){
    fitFunc->SetParName(iPar,fitParNames.at(iPar).Data());
  }

  
}

//____________________________________________________________
YieldExtractionFit::YieldExtractionFit(int particleSpecies, int yBin, int mTm0Bin,
				       TH1D *dHisto, ParticleInfo *pInfo, std::vector<TF1 *> tofFits,
				       bool blah){

  //TOF Mode
  isTPCMode = false;

  //Bin Info
  pid = particleSpecies;
  yIndex = yBin;
  mTm0Index = mTm0Bin;
  minEntries = 100;

  //Kinematic Info
  mass = particleInfo->GetParticleMass(pid);
  rapidity = GetRapidityRangeCenter(yIndex);
  mTm0 = GetmTm0RangeCenter(mTm0Index);
  mT = mTm0 + mass;
  pT = ConvertmTm0ToPt(mTm0,mass);
  pZ = ComputepZ(mT,rapidity);
  pTotal = ComputepTotal(pT,pZ);

  yield = 0.0;
  yieldErr = 0.0;

  electronAmp = -1;
  electronMax = -1;
  slushParam = -1;
   
  particleInfo = pInfo;
  dataHisto = dHisto;
  minFitRange = 99999;
  maxFitRange = -99999;

  minFitLine = NULL;
  maxFitLine = NULL;

  fitFunc = NULL;
  confInterval = NULL;
  yieldGraph = NULL;
  notYieldGraph = NULL;

  //Reuse the TofOptFit member
  tofOptFits = tofFits;
  tofWidth = .012;

  //Set the Predicted Means
  minFitRange = dataHisto->GetBinCenter(dataHisto->FindFirstBinAbove(1));
  maxFitRange = dataHisto->GetBinCenter(dataHisto->GetNbinsX()-1);
  for (unsigned int i=0; i<tofOptFits.size(); i++){
    predictedMean.push_back(particleInfo->PredictZTOF(pTotal,pid,i));
  }

  std::vector<double> sortedMean = predictedMean;
  std::sort(sortedMean.begin(),sortedMean.end());
  minFitRange = TMath::Max(sortedMean.front(),minFitRange)-10*tofWidth;
  maxFitRange = TMath::Min(sortedMean.back(),maxFitRange)+10*tofWidth;

  /*
  for (unsigned int i=0; i<predictedMean.size(); i++){
    minFitRange = TMath::Max(minFitRange,predictedMean.at(i) - 2*tofWidth);
    maxFitRange = TMath::Max(maxFitRange,predictedMean.at(i) + 2*tofWidth);
  }
  */
  
  //Create the Single Distributions
  for (unsigned int i=0; i<tofOptFits.size(); i++){

    if (!tofOptFits.at(i)){
      //maxFitRange = predictedMean.at(i) - 15*tofWidth;
      continue;
    }

    singleDistributions.push_back(new TF1(Form("%s_Fit_%s",
					       dataHisto->GetName(),
					       particleInfo->GetParticleName(i).Data()),
					  this,&YieldExtractionFit::singleDistFuncTOF,
					  -2,2,2,"YieldExtractionFit","singleDistFuncTOF"));
    singleDistributions.back()->SetParameter(0,i);
    singleDistributions.back()->SetNpx(10000);
    singleDistributions.back()->SetLineColor(particleInfo->GetParticleColor(i));
    fitParNames.push_back(TString::Format("%s",particleInfo->GetParticleSymbol(i).Data()));

    pidToFitParMap[i] = singleDistributions.size()-1;
    maxFitRange = predictedMean.at(i) + 10*tofWidth;
  }//End Loop Over Elements of TofOptFits

  //Create the Full Fit
  fitFunc = new TF1(Form("%s_Fit",dataHisto->GetName()),this,&YieldExtractionFit::yieldExtractFitTOF,
		    minFitRange,maxFitRange,singleDistributions.size(),"YieldExtractionFit","yieldExtractFitTOF");
  fitFunc->SetNpx(10000);
  
  for (int iPar=0; iPar<fitFunc->GetNpar(); iPar++){
    fitFunc->SetParName(iPar,fitParNames.at(iPar).Data());
  }

  assert(fitFunc->GetNpar() == (int)singleDistributions.size());
  
}


//_______________________________________________________
YieldExtractionFit::YieldExtractionFit(int particleSpecies, int yBin, int mTm0Bin,
				       TH1D *dHisto, ParticleInfo *pInfo){

  //Since this constructor does not take the tof Optimized histograms,
  //assume we are in tof mode
  isTPCMode = false;

  //Bin Info
  pid = particleSpecies;
  yIndex = yBin;
  mTm0Index = mTm0Bin;
  minEntries = 100;

  //Kinematic Info
  mass = particleInfo->GetParticleMass(pid);
  rapidity = GetRapidityRangeCenter(yIndex);
  mTm0 = GetmTm0RangeCenter(mTm0Index);
  mT = mTm0 + mass;
  pT = ConvertmTm0ToPt(mTm0,mass);
  pZ = ComputepZ(mT,rapidity);
  pTotal = ComputepTotal(pT,pZ);

  yield = 0.0;
  yieldErr = 0.0;

  electronAmp = -1;
  electronMax = -1;
  slushParam = -1;
   
  particleInfo = pInfo;
  dataHisto = dHisto;
  minFitRange = 99999;
  maxFitRange = -99999;

  minFitLine = NULL;
  maxFitLine = NULL;

  fitFunc = NULL;
  confInterval = NULL;
  yieldGraph = NULL;
  notYieldGraph = NULL;

  //Histogram Range
  double histMax = dataHisto->GetBinCenter(dataHisto->GetNbinsX());
  double histMin = dataHisto->GetBinCenter(1);
  double rangeOffset = 10 * 0.012;

  predictedMean.resize(3);
  for (unsigned int i=0; i<predictedMean.size(); i++){
    predictedMean.at(i) = particleInfo->PredictZTOF(pTotal,pid,i);
    if (predictedMean.at(i) > histMin + rangeOffset && predictedMean.at(i) < histMax - rangeOffset){
      minFitRange = TMath::Min(predictedMean.at(i),minFitRange);
      maxFitRange = TMath::Max(predictedMean.at(i),maxFitRange);
    }
  }
  minFitRange -= rangeOffset;
  maxFitRange += rangeOffset;

  for (unsigned int i=0; i<predictedMean.size(); i++){

    if (predictedMean.at(i) < minFitRange || predictedMean.at(i) > maxFitRange)
      continue;
    
    singleDistributions.push_back(new TF1(Form("%s_Fit_%s",
					       dataHisto->GetName(),
					       particleInfo->GetParticleName(i).Data()),
					  this,&YieldExtractionFit::singleGaussianFunc,
					  histMin,histMax,3,"YieldExtractionFit","singleGaussianFunc"));
    singleDistributions.back()->SetLineColor(particleInfo->GetParticleColor(i));
    singleDistributions.back()->SetNpx(10000);

    
  }

  
  //cout <<histMax <<" " <<histMin <<" " <<minFitRange <<" " <<maxFitRange <<endl;
  
  fitFunc = new TF1(Form("%s_Fit",dataHisto->GetName()),this,&YieldExtractionFit::yieldExtractFitTOF,
		    minFitRange,maxFitRange,singleDistributions.size()*3,"YieldExtractionFit","yieldExtractFitTOF");
  fitFunc->SetNpx(10000);

  TString parNames[] = {"#pi","#mu_{#pi}","#sigma_{#pi}",
			"K","#mu_{K}","#sigma_{K}",
			"p","#mu_{p}","#sigma_{p}",};
  for (int iPar=0; iPar<fitFunc->GetNpar(); iPar++){
    fitFunc->SetParName(iPar,parNames[iPar].Data());
  }
  

}

//_______________________________________________________
YieldExtractionFit::~YieldExtractionFit(){
  
  if (fitFunc)
    delete fitFunc;

  if (confInterval)
    delete confInterval;

  for (unsigned int i=0; i<singleDistributions.size(); i++){
    if (singleDistributions.at(i))
      delete singleDistributions.at(i);
  }

  if (yieldGraph)
    delete yieldGraph;
  if (notYieldGraph)
    delete notYieldGraph;

  if (minFitLine)
    delete minFitLine;
  if (maxFitLine)
    delete maxFitLine;

}

//_______________________________________________________
int YieldExtractionFit::Fit(){

  int status(0);
  
  if (isTPCMode)
    status = FitTPC();
  else
    status = FitTOF();

  confInterval = GetConfidenceIntervalOfFit(fitFunc);
  confInterval->SetFillColor(particleInfo->GetParticleColor(pid)-2);
  confInterval->SetFillStyle(3002);

  /*
  dataHisto->GetListOfFunctions()->AddFirst(fitFunc);
  for (unsigned int i=0; i<singleDistributions.size(); i++){
    dataHisto->GetListOfFunctions()->AddLast(singleDistributions.at(i));
  }
  */
  return status;
  
}


//_______________________________________________________
int YieldExtractionFit::FitTPC(){

  //Seed the Scale Parameters (the electron is handeled separately)
  for (unsigned int i=0; i<singleDistributions.size(); i++){
    if (finalFitAmpPars.at(i).second == true){
      fitFunc->FixParameter(i,finalFitAmpPars.at(i).first);
    }
    else {
      fitFunc->SetParameter(i,10);
      fitFunc->SetParLimits(i,0,500);
    }

    if (i == ELECTRON && finalFitAmpPars.at(i).second == false && electronMax >= 0){
      fitFunc->SetParameter(pidToFitParMap[ELECTRON],electronMax*.75);
      fitFunc->SetParLimits(pidToFitParMap[ELECTRON],electronMax*.1,electronMax);
    }
  }

  //Slush Parameter
  fitFunc->SetParameter(fitFunc->GetNpar()-1,1);
  fitFunc->SetParLimits(fitFunc->GetNpar()-1,.9,1.1);
  if (slushParam >= 0){
    fitFunc->FixParameter(fitFunc->GetNpar()-1,slushParam);
  }
    
  //If the slush parameter is not fixed, first Fit to get the slush parameter,
  //then fix the slush parameter
  else {
    dataHisto->Fit(fitFunc,"NRLQ");  
    fitFunc->FixParameter(fitFunc->GetNpar()-1,fitFunc->GetParameter(fitFunc->GetNpar()-1));
  }
  
  //Fit Again for the final time
  dataHisto->Fit(fitFunc,"NRL");

  //Set the Amplitudes of the singleDistributions
  for (unsigned int iPar=0; iPar<singleDistributions.size(); iPar++){
    singleDistributions.at(iPar)->SetParameter(1,fitFunc->GetParameter(fitFunc->GetNpar()-1));
    singleDistributions.at(iPar)->SetParameter(2,fitFunc->GetParameter(iPar));
    finalFitAmpPars.at(iPar).first = fitFunc->GetParameter(iPar);
  }
  
  return 1;

}

//_______________________________________________________
int YieldExtractionFit::FitTOF(){

  for (int iPar=0; iPar<fitFunc->GetNpar(); iPar++){
    fitFunc->SetParameter(iPar,.3);
    fitFunc->SetParLimits(iPar,0.0001,10);
  }
  
  //Do The Fit
  dataHisto->Fit(fitFunc,"NL","",minFitRange,maxFitRange);
  
  for (int i=0; i<fitFunc->GetNpar(); i++){
    singleDistributions.at(i)->SetParameter(1,fitFunc->GetParameter(i));
  }
  
  /*
  //Set the single distribution parameters
  for (unsigned int i=0; i<singleDistributions.size(); i++){
    singleDistributions.at(i)->SetParameter(1,fitFunc->GetParameter(0+i*3));
    //singleDistributions.at(i)->SetParameter(0,fitFunc->GetParameter(0+i*3));
    //singleDistributions.at(i)->SetParameter(1,fitFunc->GetParameter(1+i*3));
    //singleDistributions.at(i)->SetParameter(2,fitFunc->GetParameter(2+i*3));
    }*/
  /*
  tofFitPars.clear();
  tofFitPars.resize(fitFunc->GetNpar());
  for (int iPar=0; iPar<fitFunc->GetNpar(); iPar++){
    tofFitPars.at(iPar) = std::make_pair(fitFunc->GetParameter(iPar),false);
  }
  */
  return 1;
}

//_______________________________________________________
void YieldExtractionFit::DrawIndividualDistributions(){

  for (unsigned int i=0; i<singleDistributions.size(); i++){
    if (!singleDistributions.at(i))
      continue;
    singleDistributions.at(i)->Draw("SAME");
  }

  minFitLine = new TLine(minFitRange,dataHisto->GetMinimum(),minFitRange,1000);
  maxFitLine = new TLine(maxFitRange,dataHisto->GetMinimum(),maxFitRange,1000);

  minFitLine->SetLineColor(kBlack);
  maxFitLine->SetLineColor(kBlack);

  minFitLine->SetLineStyle(9);
  maxFitLine->SetLineStyle(9);

  minFitLine->SetLineWidth(3);
  maxFitLine->SetLineWidth(3);
  
  minFitLine->Draw("SAME");
  maxFitLine->Draw("SAME");
}

//_______________________________________________________
void YieldExtractionFit::DrawFit(TString opt){

  if (!fitFunc)
    return;

  fitFunc->Draw(opt.Data());
  
}

//_______________________________________________________
Double_t YieldExtractionFit::yieldExtractFitTPC(Double_t *x, Double_t *par){
  
  double xx = x[0];

  //All of the parameters (except the last one) are the
  //scale factors for the normalizations of the fits.
  //The last parameter is the slush (horizontal) scale factor

  //Set the Parameters of all the particles 
  for (unsigned int iPar=0; iPar<singleDistributions.size(); iPar++){
    singleDistributions.at(iPar)->SetParameter(2,par[iPar]); //Scale
    singleDistributions.at(iPar)->SetParameter(1,par[fitFunc->GetNpar()-1]); //Slush
  }

  double val(0);
  for (unsigned int i=0; i<singleDistributions.size(); i++){
    val += singleDistributions.at(i)->Eval(xx);
  }

  return val;
  

}

//_________________________________________________________
double YieldExtractionFit::yieldExtractFitTOF(Double_t *x, Double_t *par){

  double xx = x[0];

  //Set the Parameters of the Funcs
  for (unsigned int iPar=0; iPar<singleDistributions.size(); iPar++){
    singleDistributions.at(iPar)->SetParameter(1,par[iPar]);
  }

  double val(0);
  for (unsigned int i=0; i<singleDistributions.size(); i++){
    val += singleDistributions.at(i)->Eval(xx);
  }

  return val;
  
  /*
  //Set the Parameters of the Functions
  for (unsigned int i=0; i<singleDistributions.size(); i++){
    for (int iPar=0; iPar<3; iPar++){
      singleDistributions.at(i)->SetParameter(iPar,par[i*3+iPar]);
    }
  }
  
  double val(0);
  for (unsigned int i=0; i<singleDistributions.size(); i++){
    val += singleDistributions.at(i)->Eval(xx);
  }
 
  return val;
  */
}

//_________________________________________________________
Double_t YieldExtractionFit::singleDistFunc(Double_t *x, Double_t *par){

  //Parameter Definitions
  //par[0] = tofOptFit index
  //par[1] = slush factor
  //par[2] = scale factor
  
  double xx = x[0] * par[1];

  int index = TMath::Nint(fabs(par[0]));
  double scale = par[2];

  if (!tofOptFits.at(index))
    return 0;
  
  return scale * tofOptFits.at(index)->Eval(xx);
  
}

//________________________________________________________
Double_t YieldExtractionFit::singleDistFuncTOF(Double_t *x, Double_t *par){

  double xx = x[0];

  int index = TMath::Nint(fabs(par[0]));
  double scale = par[1];

  if (!tofOptFits.at(index))
    return 0;

  return scale * tofOptFits.at(index)->Eval(xx);  
}

//________________________________________________________
Double_t YieldExtractionFit::singleGaussianFunc(Double_t *x, Double_t *par){

  double xx = x[0];

  double amp = par[0];
  double mean = par[1];
  double width = par[2];

  return amp * pow(TMath::E(),-(pow(xx-mean,2) / (2.0 * pow(width,2)) ) );
  
}

//________________________________________________________
std::pair<double,double> YieldExtractionFit::ComputeYield(double nEvents, TGraphErrors *altConfInterval, TF1 *altFitFunc){

  if ( (!altConfInterval && altFitFunc) || (altConfInterval && !altFitFunc)){
    cout <<"ERROR - YieldExtractionFit::ComputeYield() - Both altConfInterval and altFitFunc must either be NULL or non-NULL. You can not mix states! EXITING!\n";
    exit (EXIT_FAILURE);
  }
  
  //Get the Fit for the particle of interest
  TF1 *pidInterestFit = singleDistributions.at(pidToFitParMap[pid]);//singleDistributions.at(pid);

  TGraphErrors *useConfInterval = NULL;
  if (altConfInterval)
    useConfInterval = altConfInterval; //Use the confidence interval that is passed in
  else
    useConfInterval = confInterval;    //Use the internal confidence interval in this (default)
  
  yieldGraph = new TGraph();
  notYieldGraph = new TGraph();

  yieldGraph->SetName(Form("%s_YieldGraph",dataHisto->GetName()));
  notYieldGraph->SetName(Form("%s_NotYieldGraph",dataHisto->GetName()));
  
  yieldGraph->SetMarkerStyle(7);
  notYieldGraph->SetMarkerStyle(7);
  
  yieldGraph->SetMarkerColor(particleInfo->GetParticleColor(pid));
  notYieldGraph->SetMarkerColor(kGray+1);
  
  //Loop Over the bins of the histogram in the range of the full fit.
  //For each bin compute the yields of the particle of interest and non interest
  Double_t minRange(0), maxRange(0);
  fitFunc->GetRange(minRange,maxRange);
  for (int iBin=dataHisto->FindBin(minRange); iBin<=dataHisto->FindBin(maxRange); iBin++){

    //Skip Empty Bins
    Double_t binEntries = dataHisto->GetBinContent(iBin);
    if (binEntries == 0)
      continue;
    
    //Ratio of full fit to interest fit in this bin
    Double_t binCenter = dataHisto->GetBinCenter(iBin);
    Double_t ratio = pidInterestFit->Eval(binCenter) / fitFunc->Eval(binCenter);
    
    //Since we are fitting histograms there empty bins can result in a nan for the ratio
    //so we need to protect agains it.
    if (ratio == 0.00)
      continue;
    
    Double_t binInterestYield = ratio * binEntries;
    Double_t binConfoundYield = (1.0-ratio) * dataHisto->GetBinContent(iBin);

    yieldGraph->SetPoint(yieldGraph->GetN(),binCenter,binInterestYield);
    notYieldGraph->SetPoint(notYieldGraph->GetN(),binCenter,binConfoundYield);

    Double_t fractionalErr(1);
    if (altFitFunc){
      fractionalErr = GetErrorAtValue(useConfInterval,binCenter) / altFitFunc->Eval(binCenter);
    }
    else
      fractionalErr = GetErrorAtValue(useConfInterval,binCenter) / fitFunc->Eval(binCenter);
    
    Double_t ratioErr = ratio * fractionalErr;
    Double_t binErr = dataHisto->GetBinError(iBin);
    Double_t binYieldTotalErr = binInterestYield * sqrt(pow(binErr/binEntries,2) + pow(ratioErr/ratio,2));
    
    yield += binInterestYield;
    yieldErr += binYieldTotalErr * binYieldTotalErr;

   
  }
  
  //Compute the Error
  yieldErr = sqrt(yieldErr);

  
  Double_t pi = TMath::Pi();
  Double_t normFactor = (1.0/mT) * (1.0/mTm0BinWidth) * (1.0/rapidityBinWidth) *
    (1.0/nEvents) * (1.0/(2.0*pi));
  
  yield = yield * normFactor;
  yieldErr = yieldErr * normFactor;

  return std::make_pair(yield,yieldErr); 

}


//_________________________________________________________
void YieldExtractionFit::FixAmpParameter(int particle, double val){

  int fitParIndex = pidToFitParMap[particle];

  finalFitAmpPars.at(fitParIndex).first = val;
  finalFitAmpPars.at(fitParIndex).second = true;

}

//_________________________________________________________
double YieldExtractionFit::GetAmpParameter(int particle){

  int fitParIndex = pidToFitParMap[particle];

  return finalFitAmpPars.at(fitParIndex).first;

}

//_________________________________________________________
double YieldExtractionFit::GetSlushParameter(){
  return fitFunc->GetParameter(fitFunc->GetNpar()-1);
}

//_________________________________________________________
double YieldExtractionFit::ComputeTotalError(TGraphErrors *confInterval){

}
