#!/bin/bash

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunFeeddownBackgroundAnalysis.bash"
    echo "    PURPOSE:"
    echo "        This estimates the feed down background contaimination for pions"
    echo "        kaons, and protons. This assumes that the background simulations have"
    echo "        already been done and processed. See StURQMD and MiniMcReader repo."
    echo "    USAGE:"
    echo "        ./RunFeeddownBackgroundAnalysis.bash [OPTION] ... [CONFIG FILE]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configuration file - name of configuration file with path if applicable"
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo ""
    echo ""
}

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "h" opts; do
	case "$opts" in
	    h) help_func; exit 1;;
	    ?) exit 1;;
	    *) echo "For help use option: -h"; exit 1;;
	esac
	shift
	OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
	POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
	shift
	OPTIND=1
    fi
done

#Make sure only one argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: This script requires only one argument. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

#Assign user inputs to variables
configFile=${POSITIONALPARAM[0]}

#Make Sure the configuration variable is set
if [ -z $configFile ]; then
    echo "ERROR: Configuration file was not set. For usage information use option -h"
    exit 1
fi

#Make Sure the configuration file exists
if [ ! -e $configFile ]; then
    echo "ERROR: Configruation file ($configFile) was not found!"
    exit 1
fi

#Get the Configuration from the Config file
source $configFile

nice root -l -b -q ../macros/RunFeedDownBackgroundAnalysis.C\(\"$backgroundSimFile\",\"$correctionFile\",\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/FeeddownBackgroundAnalysis_$eventConfig.log 2>&1


exit 0
