#!/bin/bash                                                                                                          

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunMakeEmbeddingCorrectionCurves.bash"
    echo "    PURPOSE:"
    echo "        This reads the embedding files from minimcreader and constructs the"
    echo "        efficiency and energy loss graphs for the different particle species,"
    echo "        centrality bins, and rapidity bins."
    echo "    USAGE:"
    echo "        ./RunMakeEmbeddingCorrectionCurves.bash [OPTION] ... [CONFIG FILE]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configuration file - name of configuration file with path if applicable"
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo "        -p - only do this particle species index (see ParticleInfo for indices)"
    echo ""
    echo ""

}

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "p:h" opts; do
        case "$opts" in
            p) userParticleSpecies="${OPTARG}"; shift;;
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi
done

#Make sure only one argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: This script requires only one argument. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

#Assign user inputs to variables
configFile=${POSITIONALPARAM[0]}

#Make Sure the configuration variable is set
if [ -z $configFile ]; then
    echo "ERROR: Configuration file was not set. For usage information use option -h"
    exit 1
fi

#Make Sure the configuration file exists
if [ ! -e $configFile ]; then
    echo "ERROR: Configruation file ($configFile) was not found!"
    exit 1
fi

#Get the Configuration from the Config file
source $configFile

#If the user has specified a particular particle species
if [ ! -z "$userParticleSpecies" ]; then
    if [ "$userParticleSpecies" -eq 0 ]; then
	root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_PionMinus_$eventConfig\_Embedding.root\",\"$correctionFile\",0,-1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_PionMinus_$eventConfig.log 2>&1
	root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_PionPlus_$eventConfig\_Embedding.root\",\"$correctionFile\",0,1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_PionPlus_$eventConfig.log 2>&1
    elif [ "$userParticleSpecies" -eq 1 ]; then
	root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_KaonMinus_$eventConfig\_Embedding.root\",\"$correctionFile\",1,-1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_KaonMinus_$eventConfig.log 2>&1
	root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_KaonPlus_$eventConfig\_Embedding.root\",\"$correctionFile\",1,1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_KaonPlus_$eventConfig.log 2>&1
    elif [ "$userParticleSpecies" -eq 2 ]; then
	root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_ProtonMinus_$eventConfig\_Embedding.root\",\"$correctionFile\",2,-1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_ProtonMinus_$eventConfig.log 2>&1
	root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_ProtonPlus_$eventConfig\_Embedding.root\",\"$correctionFile\",2,1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_ProtonPlus_$eventConfig.log 2>&1
    fi

else
    root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_PionMinus_$eventConfig\_Embedding.root\",\"$correctionFile\",0,-1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_PionMinus_$eventConfig.log 2>&1
    root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_PionPlus_$eventConfig\_Embedding.root\",\"$correctionFile\",0,1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_PionPlus_$eventConfig.log 2>&1
    root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_KaonMinus_$eventConfig\_Embedding.root\",\"$correctionFile\",1,-1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_KaonMinus_$eventConfig.log 2>&1
    root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_KaonPlus_$eventConfig\_Embedding.root\",\"$correctionFile\",1,1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_KaonPlus_$eventConfig.log 2>&1
    root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_ProtonMinus_$eventConfig\_Embedding.root\",\"$correctionFile\",2,-1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_ProtonMinus_$eventConfig.log 2>&1
    root -l -b -q ../macros/RunMakeEmbeddingCorrectionCurves.C\(\"$embDir/$name\_ProtonPlus_$eventConfig\_Embedding.root\",\"$correctionFile\",2,1,\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/EmbeddingCorrectionCurves_ProtonPlus_$eventConfig.log 2>&1
fi

exit 0

