#!/bin/bash

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunGenerateRawSpectra.bash"
    echo "    PURPOSE:"
    echo "        This fits the zTPC and zTOF distributions in the yield histogram file"
    echo "        and extract with the yields of pi,k,p for various centrality, rapidity,"
    echo "        and mT-m0 bins. The output is a root file containing the raw spectra."
    echo "    USAGE:"
    echo "        ./RunGenerateRawSpectra.bash [OPTION] ... [CONFIG FILE]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configuration file - name of configuration file with path if applicable"
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo "        -p - integer for the desired particle species (PION=0, KAON=1, PROTON=2) (default is to do all)"
    echo "        -c - integer for the desired centrality bin (default is to do all bins)"
    echo "        -y - double for the desired rapidity bin (default is to do all bins"
    echo ""
    echo ""
}

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "p:c:y:h" opts; do
	case "$opts" in
	    p) particleSpecies="${OPTARG}"; shift;;
	    c) centralityIndex="${OPTARG}"; shift;;
	    y) rapidityValue="${OPTARG}"; shift;;
	    h) help_func; exit 1;;
	    ?) exit 1;;
	    *) echo "For help use option: -h"; exit 1;;
	esac
	shift
	OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
	POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
	shift
	OPTIND=1
    fi
done

#Make sure only one argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: This script requires only one argument. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

#Assign user inputs to variables
configFile=${POSITIONALPARAM[0]}

#Make Sure the configuration variable is set
if [ -z $configFile ]; then
    echo "ERROR: Configuration file was not set. For usage information use option -h"
    exit 1
fi

#Make Sure the configuration file exists
if [ ! -e $configFile ]; then
    echo "ERROR: Configruation file ($configFile) was not found!"
    exit 1
fi

#Get the Configuration from the Config file
source $configFile

species=-999
centBin=-999
rapidity=-999

if [ ! -z "$particleSpecies" ]; then
   species=$particleSpecies
fi

if [ ! -z "$centralityIndex" ]; then
    centBin=$centralityIndex
fi

if [ ! -z "$rapidityValue" ]; then
    rapidity=$rapidityValue
fi

processID=()
outFiles=()

if [ "$species" -ne -999 ]; then
    outFiles+=($tmpDir/spectra_All.root)
    nice root -l -q ../macros/RunGenerateRawSpectra.C\(\"$yieldHistoFile\",\"${outFiles[0]}\",\"$pidCalibrationFile\",\"$starLib\",$energy,\"$eventConfig\",$species,$centBin,$rapidity\) > $logDir/GenerateRawSpectra.log 2>&1
    processID+=($!)
else
    outFiles+=($tmpDir/spectra_Pion.root)
    nice root -b -q -l ../macros/RunGenerateRawSpectra.C\(\"$yieldHistoFile\",\"${outFiles[0]}\",\"$pidCalibrationFile\",\"$starLib\",$energy,\"$eventConfig\",0,$centBin,$rapidity\) > $logDir/GenerateRawSpectra_Pion.log 2>&1 &
    processID+=($!)

    outFiles+=($tmpDir/spectra_Kaon.root)
    nice root -b -q -l ../macros/RunGenerateRawSpectra.C\(\"$yieldHistoFile\",\"${outFiles[1]}\",\"$pidCalibrationFile\",\"$starLib\",$energy,\"$eventConfig\",1,$centBin,$rapidity\) > $logDir/GenerateRawSpectra_Kaon.log 2>&1 &
    processID+=($!)

    outFiles+=($tmpDir/spectra_Proton.root)
    nice root -b -q -l ../macros/RunGenerateRawSpectra.C\(\"$yieldHistoFile\",\"${outFiles[2]}\",\"$pidCalibrationFile\",\"$starLib\",$energy,\"$eventConfig\",2,$centBin,$rapidity\) > $logDir/GenerateRawSpectra_Proton.log 2>&1 &
    processID+=($!)
fi

wait ${processID[@]}

hadd -f $spectraFile ${outFiles[@]}

wait

rm ${outFiles[@]}

exit 0
