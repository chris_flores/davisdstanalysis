#!/bin/bash

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunTofSkimmerAndBinner.bash"
    echo "    PURPOSE:"
    echo "        This create ZTPC and ZTOF Histograms, binning the tracks by"
    echo "        particle speicies, charge, centrality, rapidity, and mTm0."
    echo "    USAGE:"
    echo "        ./RunGenerateCentralityDatabase.bash [OPTION] ... [CONFIG FILE]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configuration file - name of configuration file with path if applicable"
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo ""
    echo ""
    
}

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "n:h" opts; do
        case "$opts" in
            n) nUserEvents="${OPTARG}"; shift;;
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi
done

#Make sure only one argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: This script requires only one argument. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

#Assign user inputs to variables
configFile=${POSITIONALPARAM[0]}

#Make Sure the configuration variable is set
if [ -z $configFile ]; then
    echo "ERROR: Configuration file was not set. For usage information use option -h"
    exit 1
fi

#Make Sure the configuration file exists
if [ ! -e $configFile ]; then
    echo "ERROR: Configruation file ($configFile) was not found!"
    exit 1
fi

#Get the Configuration from the Config file
source $configFile

#Variables used below
processID=()
outFiles=()

for i in ${adcFiles[@]}; do

    rndStr=$(cat /dev/urandom | tr -dc 'A-Z0-9' | fold -w 12 | head -n 1)
    tempOutFile=$tmpDir/$rndStr.root
    outFiles+=($tempOutFile)

    nice root -l -b -q ../macros/RunGenerateCentralityDatabase.C\(\"$i\",\"$tempOutFile\",\"$starLib\",$energy,\"$eventConfig\"\) > $logDir/GenerateCentralityDatabase\_$rndStr.log 2>&1 &

    processID+=($!)

done
wait ${processID[@]}

hadd -f $centDatabaseFile ${outFiles[@]}

rm ${outFiles[@]}

exit 0

